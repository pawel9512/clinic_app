# Employee
#
admin = Employee.create!(first_name: "Paweł", last_name: 'Suchta', role: :admin, email: 'admin@wp.pl', password: 'qweqweqwe', password_confirmation: 'qweqweqwe') unless Employee.find_by(email: 'admin@wp.pl').present?
doctor1 = Employee.create!(first_name: "Andrzej", last_name: 'Korona', role: :doctor, email: 'doctor1@wp.pl', password: 'qweqweqwe', password_confirmation: 'qweqweqwe', pesel: '1234341342', appointment_time: 30) unless Employee.find_by(email: 'doctor1@wp.pl').present?
doctor2 = Employee.create!(first_name: "Rick", last_name: 'Sanchez', role: :doctor, email: 'doctor2@wp.pl', password: 'qweqweqwe', password_confirmation: 'qweqweqwe', pesel: '14353341342', appointment_time: 30) unless Employee.find_by(email: 'doctor2@wp.pl').present?
receptionist = Employee.create!(first_name: "Halina", last_name: 'Pke', role: :receptionist, email: 'receptionist@wp.pl', password: 'qweqweqwe', password_confirmation: 'qweqweqwe', pesel: '23353341342') unless Employee.find_by(email: 'receptionist@wp.pl').present?


# Patients
#
patient1 = Patient.create!(first_name: "Bogusz", last_name: 'Doma', email: 'patient1@wp.pl', password: 'qweqweqwe', password_confirmation: 'qweqweqwe', identifier: '2343441342', sex: :male, blood_group: :a_plus) unless Patient.find_by(email: 'patient1@wp.pl').present?
patient2 = Patient.create!(first_name: "Wacław", last_name: 'Suchta', email: 'patient2@wp.pl', password: 'qweqweqwe', password_confirmation: 'qweqweqwe', identifier: '23334844342', sex: :male, blood_group: :b_plus) unless Patient.find_by(email: 'patient2@wp.pl').present?
