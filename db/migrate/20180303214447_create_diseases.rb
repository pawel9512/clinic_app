class CreateDiseases < ActiveRecord::Migration[5.1]
  def up

    create_table :diseases, id: false do |t|
      t.string :name
      t.string :code, null: false
      t.integer :type
      t.text :description
      t.timestamps
    end
    execute "ALTER TABLE diseases ADD PRIMARY KEY (code);"
  end

  def down
    execute "ALTER TABLE diseases DROP CONSTRAINT diseases_pkey ;"
    drop_table :diseases
  end
end
