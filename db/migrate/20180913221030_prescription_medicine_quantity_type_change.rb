class PrescriptionMedicineQuantityTypeChange < ActiveRecord::Migration[5.1]
  def change
    change_column :prescription_medicines, :quantity, :integer
  end
end
