class AddSlugToDiseases < ActiveRecord::Migration[5.1]
  def change
    add_column :diseases, :slug, :string, unique: true
  end
end
