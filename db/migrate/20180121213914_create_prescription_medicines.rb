class CreatePrescriptionMedicines < ActiveRecord::Migration[5.1]
  def change
    create_table :prescription_medicines do |t|
      t.references :prescription, index: true, foreign_key: true
      t.references :medicine,index: true, foreign_key: true
      t.integer :quantity
      t.string :comment

      t.timestamps
    end
  end
end
