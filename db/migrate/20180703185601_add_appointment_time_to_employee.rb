class AddAppointmentTimeToEmployee < ActiveRecord::Migration[5.1]
  def change
    add_column :employees, :appointment_time, :integer
  end
end
