class AddAncestryToDisease < ActiveRecord::Migration[5.1]
  def change
    add_column :diseases, :ancestry, :string
    add_column :diseases, :codes_path, :string
    add_index :diseases, :ancestry
    add_index :diseases, :codes_path, unique: true
  end
end
