class CreateAppointments < ActiveRecord::Migration[5.1]
  def change
    create_table :appointments do |t|
      t.references :patient, foreign_key: true
      t.references :doctor
      t.references :receptionist
      t.datetime :planned_start
      t.datetime :planned_end
      t.datetime :actual_start
      t.datetime :actual_end
      t.integer :status
      t.string :description

      t.timestamps

    end
    add_foreign_key :appointments, :employees, column: :doctor_id
    add_foreign_key :appointments, :employees, column: :receptionist_id
  end
end
