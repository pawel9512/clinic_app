class AddCommentColumnToAppointment < ActiveRecord::Migration[5.1]
  def change
    add_column :appointments, :doctor_comment, :string
  end
end
