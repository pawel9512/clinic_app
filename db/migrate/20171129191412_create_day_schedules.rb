class CreateDaySchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :day_schedules do |t|
      t.references :employee, index: true, foreign_key: true
      t.integer :day
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps null: false
    end
  end
end
