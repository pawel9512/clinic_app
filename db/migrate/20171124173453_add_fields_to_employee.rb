class AddFieldsToEmployee < ActiveRecord::Migration[5.1]
  def change
    add_column :employees, :role, :integer
    add_column :employees, :first_name, :string
    add_column :employees, :last_name, :string
    add_column :employees, :pesel, :string
    add_column :employees, :phone_number, :string
  end
end
