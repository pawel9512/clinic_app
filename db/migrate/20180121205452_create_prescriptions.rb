class CreatePrescriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :prescriptions do |t|
      t.references :appointment, foreign_key: true
      t.references :patient, foreign_key: true
      t.references :employee, foreign_key: true
      t.string :description

      t.timestamps
    end
  end
end
