class CreateAppointmentDiseases < ActiveRecord::Migration[5.1]
  def change
    create_table :appointment_diseases do |t|
      t.string :disease_code
      t.references :appointment, foreign_key: true

      t.timestamps
    end
    add_foreign_key :appointment_diseases, :diseases, column: :disease_code, primary_key: :code
    add_index :appointment_diseases, [:appointment_id, :disease_code], name: 'appointment_disease_index'
  end

end
