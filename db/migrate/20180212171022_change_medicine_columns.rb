class ChangeMedicineColumns < ActiveRecord::Migration[5.1]
  def change
    add_column :medicines, :common_name, :string
    add_column :medicines, :package, :string
    add_column :medicines, :marketing_holder, :string
    add_column :medicines, :leaflet_link, :string
    add_column :medicines, :characteristic_link, :string
  end
end
