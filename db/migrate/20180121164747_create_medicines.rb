class CreateMedicines < ActiveRecord::Migration[5.1]
  def change
    create_table :medicines do |t|
      t.string :name
      t.string :active_substance
      t.string :strength
      t.string :form

      t.timestamps
    end
  end
end
