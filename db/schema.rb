# This file is auto-generated from the current state of the database. Instead
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180913221030) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "voivodeship"
    t.string "county"
    t.string "borough"
    t.string "town"
    t.string "street"
    t.string "postal_code"
    t.string "building_nr"
    t.integer "flat_nr"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "appointment_diseases", force: :cascade do |t|
    t.string "disease_code"
    t.bigint "appointment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["appointment_id", "disease_code"], name: "appointment_disease_index"
    t.index ["appointment_id"], name: "index_appointment_diseases_on_appointment_id"
  end

  create_table "appointments", force: :cascade do |t|
    t.bigint "patient_id"
    t.bigint "doctor_id"
    t.bigint "receptionist_id"
    t.datetime "planned_start"
    t.datetime "planned_end"
    t.datetime "actual_start"
    t.datetime "actual_end"
    t.integer "status"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "doctor_comment"
    t.index ["doctor_id"], name: "index_appointments_on_doctor_id"
    t.index ["patient_id"], name: "index_appointments_on_patient_id"
    t.index ["receptionist_id"], name: "index_appointments_on_receptionist_id"
  end

  create_table "day_schedules", force: :cascade do |t|
    t.bigint "employee_id"
    t.integer "day"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id"], name: "index_day_schedules_on_employee_id"
  end

  create_table "diseases", primary_key: "code", id: :string, force: :cascade do |t|
    t.string "name"
    t.integer "type"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ancestry"
    t.string "codes_path"
    t.string "slug"
    t.index ["ancestry"], name: "index_diseases_on_ancestry"
    t.index ["codes_path"], name: "index_diseases_on_codes_path", unique: true
  end

  create_table "employees", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role"
    t.string "first_name"
    t.string "last_name"
    t.string "pesel"
    t.string "phone_number"
    t.integer "appointment_time"
    t.index ["email"], name: "index_employees_on_email", unique: true
    t.index ["reset_password_token"], name: "index_employees_on_reset_password_token", unique: true
  end

  create_table "employments", force: :cascade do |t|
    t.bigint "employee_id"
    t.datetime "hire_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id"], name: "index_employments_on_employee_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "medicines", force: :cascade do |t|
    t.string "name"
    t.string "active_substance"
    t.string "strength"
    t.string "form"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "common_name"
    t.string "package"
    t.string "marketing_holder"
    t.string "leaflet_link"
    t.string "characteristic_link"
  end

  create_table "patients", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "first_name"
    t.string "last_name"
    t.integer "role"
    t.integer "identifier_type"
    t.string "identifier"
    t.integer "sex"
    t.integer "blood_group"
    t.datetime "birth_date"
    t.string "phone_number"
    t.bigint "address_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["address_id"], name: "index_patients_on_address_id"
    t.index ["email"], name: "index_patients_on_email", unique: true
    t.index ["reset_password_token"], name: "index_patients_on_reset_password_token", unique: true
  end

  create_table "prescription_medicines", force: :cascade do |t|
    t.bigint "prescription_id"
    t.bigint "medicine_id"
    t.integer "quantity"
    t.string "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["medicine_id"], name: "index_prescription_medicines_on_medicine_id"
    t.index ["prescription_id"], name: "index_prescription_medicines_on_prescription_id"
  end

  create_table "prescriptions", force: :cascade do |t|
    t.bigint "appointment_id"
    t.bigint "patient_id"
    t.bigint "employee_id"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["appointment_id"], name: "index_prescriptions_on_appointment_id"
    t.index ["employee_id"], name: "index_prescriptions_on_employee_id"
    t.index ["patient_id"], name: "index_prescriptions_on_patient_id"
  end

  add_foreign_key "appointment_diseases", "appointments"
  add_foreign_key "appointment_diseases", "diseases", column: "disease_code", primary_key: "code"
  add_foreign_key "appointments", "employees", column: "doctor_id"
  add_foreign_key "appointments", "employees", column: "receptionist_id"
  add_foreign_key "appointments", "patients"
  add_foreign_key "day_schedules", "employees"
  add_foreign_key "employments", "employees"
  add_foreign_key "prescription_medicines", "medicines"
  add_foreign_key "prescription_medicines", "prescriptions"
  add_foreign_key "prescriptions", "appointments"
  add_foreign_key "prescriptions", "employees"
  add_foreign_key "prescriptions", "patients"
end
