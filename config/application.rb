require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Angle
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Warsaw'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.assets.compile = true
    config.i18n.default_locale = :pl
    config.autoload_paths += %W(#{config.root}/app/workers)
    # setup npm folder for lookup
    config.assets.paths << Rails.root.join('vendor', 'assets', 'node_modules')
    # fonts
    config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/
    # images
    config.assets.precompile << /\.(?:png|jpg)$/
    # precompile vendor assets
    config.assets.precompile += %w( base.js )
    config.assets.precompile += %w( base.css font_path_overwrite.css )
    config.assets.precompile += %w( pdf.css )
    config.assets.precompile += %w( pdf.js )
    # precompile themes
    config.assets.precompile += ['angle/themes/theme-a.css',
                                 'angle/themes/theme-b.css',
                                 'angle/themes/theme-c.css',
                                 'angle/themes/theme-d.css',
                                 'angle/themes/theme-e.css',
                                 'angle/themes/theme-f.css',
                                 'angle/themes/theme-g.css',
                                 'angle/themes/theme-h.css'
    ]
    # Controller assets
    config.assets.precompile += [
        # Scripts
        'singleview.js',
        'employee/sessions.js',
        'employee/employees.js',
        'employee/day_schedules.js',
        'employee/patients.js',
        'employee/medicines.js',
        'employee/appointments.js',
        'employee/diseases_categories.js',
        'employee/diseases_subcategories.js',
        'employee/main_diseases.js',
        'employee/diseases.js',
        'employee/dashboard.js',
        'employee/diseases.js',
        'employee/reports.js',


        # Stylesheets
        'singleview.css',
        'employee/sessions.css',
        'employee/employees.css',
        'employee/day_schedules.css',
        'employee/patients.css',
        'employee/medicines.css',
        'employee/appointments.css',
        'employee/diseases_categories.css',
        'employee/diseases_subcategories.css',
        'employee/main_diseases.css',
        'employee/diseases.css',
        'employee/dashboard.css',
        'employee/diseases.css',
        'employee/reports.css',


        #Patient
        #CSS
        'patient/sessions.css',
        'patient/dashboard.css',
        'patient/employees.css',
        'patient/appointments.css',
        'patient/day_schedules.css',
        'patient/medicines.css',
        'patient/diseases.css',
        'patient/reports.css',

        #JAVASCRIPTS
        #
        'patient/sessions.js',
        'patient/dashboard.js',
        'patient/employees.js',
        'patient/appointments.js',
        'patient/day_schedules.js',
        'patient/medicines.js',
        'patient/diseases.js',
        'patient/reports.js'


    ]


  end
end


