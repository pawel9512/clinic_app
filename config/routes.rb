Rails.application.routes.draw do

  require 'sidekiq/web'
  authenticate :employee do
    mount Sidekiq::Web => '/sidekiq'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # defaults to dashboard
  root :to => 'singleview#index'

  # view routes
  get '/singleview' => 'singleview#index'

  # api routes
  get '/api/i18n/:locale' => 'api#i18n'

  namespace :employee do
    root 'dashboard#welcome'
    resources :employments
    resources :patients do
      get :appointments, to: 'appointments#patient_appointments'
    end
    resources :employees do
      get :doctors, on: :collection
      resources :appointments, only: [:index, :edit]
      post 'appointments-doctors', on: :collection
      get 'appointments', on: :collection
      resources :day_schedules
      member do
        get 'work-schedule', to: 'employees#work_schedule'
      end
    end
    resources :appointments, only: [:create, :update, :destroy, :show] do
      post :generate_appointments, path: 'generate-appointments', on: :collection
      post :day_appointments, path: 'day-appointments', on: :collection
      get :summary, on: :member
      put :cancel, on: :member
      get :incoming, on: :collection
    end
    get :appointments, to: 'appointments#appointments'
    resources :medicines do
      get 'fetch-from-csv', on: :collection
      post 'parse-csv', on: :collection
    end
    resources :specific_diseases, path: 'specific-diseases', only: [:index, :edit, :show, :update, :destroy]
    resources :diseases, only: [:index, :show, :edit, :create, :destroy, :update] do
      collection do
        get :import
        get 'specific-diseases', to: 'diseases#specific_diseases'
        post 'parse-xml'
        get '/:id/new', to: 'diseases#new', as: :new
        get '/:id/details', to: 'diseases#details', as: :details
        get '/new', to: 'diseases#new', as: :new_root
        post '/:id', to: 'diseases#create', as: :create
        post '/', to: 'diseases#create', as: :create_root
        get '/:id/edit', to: 'diseases#edit', as: :edit
        #   get '/new', to: 'diseases#new', as: :new_root
        get '/edit', to: 'diseases#new', as: :edit_root
        #   get '/*code', to: 'diseases#show', as: :show
        #
      end

    end
    get 'reports/doctors'
    get 'reports/patients'
    post 'reports/patient_report'


  end

  namespace :patient do
    root 'dashboard#index'
    resources :appointments, only: [:index, :update] do
      get :make_appointment, on: :collection, path: 'make-appointment'
      post :day_appointments, on: :collection, path: 'day-appointments'
      put :cancel, on: :member
      get :summary, on: :member
    end
    resources :dashboard, only: [:index]
    resources :employees, except: [:index, :new, :create, :update, :edit, :destroy, :show] do
      get :work_schedules, on: :collection, path: 'work-schedules'
      get :show_work_schedule, on: :member, path: 'work-schedule'
      post :doctors_who_has_appointments, on: :collection, path: 'doctors-who-has-appointments'
      resources :day_schedules, only: [:index]
    end
    resources :medicines, only: [:index, :show]
    resources :diseases, only: [:index, :show] do
      collection do
        get '/:id/details', to: 'diseases#details', as: :details
      end
    end
    get 'reports/patients'
    post 'reports/patient_report'
  end
  devise_for :employees, path: 'employee', controllers: {sessions: 'employee/sessions'}
  devise_for :patients, path: 'patient', controllers: {sessions: 'patient/sessions'}

end
