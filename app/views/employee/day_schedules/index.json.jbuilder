json.array! @day_schedules do |day_schedule|
  date_format = "#{Date.parse(day_schedule.day)}T%H:%M:%S"

  json.id day_schedule.id
  json.start day_schedule.start_time.strftime(date_format)
  json.end day_schedule.end_time.strftime(date_format)
  json.day day_schedule.day

  json.update_url employee_employee_day_schedule_path(@employee, day_schedule, method: :patch)
  json.edit_url edit_employee_employee_day_schedule_path(@employee, day_schedule)

end