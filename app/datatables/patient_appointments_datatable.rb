 class PatientAppointmentsDatatable < ApplicationDatatable
  delegate :summary_employee_appointment_path, to: :@view
  delegate :can?, to: :@current_ability

  def count
    Appointment.count
  end

  def total_entries
    appointments.total_count
  end

  def data
    appointments.map do |appointment|
      [].tap do |column|
        column << appointment.doctor.full_name
        column << appointment.actual_start.localtime.strftime("%d-%m-%Y %H:%M")
        column << appointment.actual_end.localtime.strftime("%H:%M")
        column << appointment.diseases.count
        column << appointment.prescriptions.count
        if can? :summary, appointment
          column << link_to("Wizyta", summary_employee_appointment_path(appointment), class: 'btn btn-info btn-xs')
        else
          column << ''
        end
      end
    end
  end

  def appointments
    @appointments ||= fetch_appointments
  end

  def do_appointments_query(patient, sort_column, sort_direction)

    if sort_column == 'diseases_count'
      return patient.appointments.left_joins(:diseases).ended.group(:id).order("COUNT(diseases.code) #{sort_direction}")
    elsif sort_column == 'prescriptions_count'
      return patient.appointments.left_joins(:prescriptions).ended.group(:id).order("COUNT(prescriptions.id) #{sort_direction}")

    end
    patient.appointments.joins(:doctor).ended.order("#{sort_column} #{sort_direction}")

  end


  def fetch_appointments

    patient = Patient.find(params[:patient_id])
    appointments = do_appointments_query(patient, sort_column, sort_direction)
    appointments = appointments.page(page).per(per_page)
    doctor_appointments = appointments.joins(:doctor).where('LOWER(first_name || \' \' || last_name) like :search', search: "%#{params[:search][:value].downcase}%")
    return doctor_appointments if doctor_appointments.present?
    splitted_date = params[:search][:value].split('-')
    if splitted_date.count < 3 or !Date.valid_date?(splitted_date[2].to_i, splitted_date[1].to_i, splitted_date[0].to_i)
      return appointments
    end
    appointments.where('actual_start::date = :search', search: params[:search][:value].to_date)
  end

  def columns
    ["LOWER(first_name || \' \' || last_name)", 'actual_start', 'actual_end', 'diseases_count', 'prescriptions_count']
  end


end