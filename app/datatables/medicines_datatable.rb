class MedicinesDatatable < ApplicationDatatable
  delegate :edit_employee_medicine_path, to: :@view
  delegate :current_employee, to: :@view
  delegate :current_patient, to: :@view

  def count
    Medicine.count
  end

  def total_entries
    medicines.total_count
  end

  def data
    medicines.map do |medicine|
      [].tap do |column|
        column << link_to(medicine.name, [:employee, medicine]) if current_employee.present?
        column << link_to(medicine.name, [:patient, medicine]) if current_patient.present?
        column << medicine.common_name
        column << medicine.active_substance
        column << medicine.marketing_holder
        links = []
        links << link_to('Edycja', edit_employee_medicine_path(medicine), class: 'btn btn-info btn-xs')
        links << link_to('Usuń', [:employee, medicine], method: :delete, class: 'btn btn-danger btn-xs', data: {confirm: "Jesteś pewien?"})
        column << links.join(' ')
      end
    end
  end

  def medicines
    @medicines ||= fetch_medicines
  end

  def fetch_medicines
    search_string = []
    columns.each do |term|
      search_string << "lower(#{term}) like :search"
    end
    medicines = Medicine.order("#{sort_column} #{sort_direction}")
    medicines = medicines.page(page).per(per_page)
    medicines.where(search_string.join(' or '), search: "%#{params[:search][:value].downcase}%")
  end

  def columns
    %w(name common_name active_substance marketing_holder)
  end
end