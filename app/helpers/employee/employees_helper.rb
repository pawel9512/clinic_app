module Employee::EmployeesHelper
  def roles_select
    Employee.roles.map { |key, value| [key.humanize, key] }
  end

  def role_pl(role)
    case role
      when 'admin'
        'Administrator'
      when 'doctor'
        'Lekarz'
      else
        'Recepcja'
    end
  end
end
