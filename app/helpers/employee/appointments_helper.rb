module Employee::AppointmentsHelper

  def time_diference_incoming(now, appointment_date)
    incoming_header = 'Nadzchodząca wizyta za'
    late_header = 'Opóźnienie wizyty o'
    minutes_time_diff = ((appointment_date - now) / 1.minute).round
    if minutes_time_diff > 0
      if minutes_time_diff < 60
        "#{incoming_header} #{minutes_time_diff.round} minut"
      elsif minutes_time_diff >= 60 and minutes_time_diff < (60 * 24)
        "#{incoming_header} #{(minutes_time_diff) / 60} godzin"
      else
        "#{incoming_header} #{minutes_time_diff / (60 * 24)} dni"

      end
    else
      if minutes_time_diff > -60
        "#{late_header} #{minutes_time_diff.round * -1} minut"
      elsif minutes_time_diff <= -60 and minutes_time_diff > (-60 * 24)
        "#{late_header} #{(minutes_time_diff) / -60} godzin"
      else
        "#{late_header} #{minutes_time_diff / (-60 * 24)} dni"

      end
    end
  end

  def summary_appointment_duration(appointment)
    seconds_diff = (appointment.actual_start - appointment.actual_end).to_i.abs

    hours = seconds_diff / 3600
    seconds_diff -= hours * 3600

    minutes = seconds_diff / 60
    seconds_diff -= minutes * 60

    seconds = seconds_diff

    "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"


  end

  def get_appointment_midicines_count(appointment)
    count = 0
    appointment.prescriptions.map do |p|
      count += p.medicines.count
    end
    count
  end


  def appointment_name_link(appointment)
    if appointment.status == 'incoming'
      edit_employee_employee_appointment_path(params[:employee_id], appointment)
    else
      summary_employee_appointment_path(appointment)
    end
  end

  def patient_appointment_name_link(appointment)
    if appointment.status == 'incoming'
      edit_employee_employee_appointment_path(params[:employee_id], appointment)
    else
      summary_employee_appointment_path(appointment)
    end
  end
end
