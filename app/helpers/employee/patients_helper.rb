module Employee::PatientsHelper
  def sex_select
    Patient.sexes.map {|key, value| [(key == 'male' ? 'Mężczyzna' : 'Kobieta'), key]}
  end

  def identifier_type_select
    Patient.identifier_types.map do |key, value|
      type = case key
               when 'pesel'
                 "Pesel"
             end
      [type, key]
    end
  end

  def blood_group_select
    hash = {
        'Plus' => [],
        'Minus' => []
    }
    Patient.blood_groups.each do |key, value|
      blood = case key
                when 'a_plus'
                  hash['Plus'].push(["A+", key])
                when 'b_plus'
                  hash['Plus'].push(["B+", key])
                when 'ab_plus'
                  hash['Plus'].push(["AB+", key])
                when 'zero_plus'
                  hash['Plus'].push(["0+", key])
                when 'a_minus'
                  hash['Minus'].push(["A-", key])
                when 'b_minus'
                  hash['Minus'].push(["B-", key])
                when 'ab_minus'
                  hash['Minus'].push(["AB-", key])
                when 'zero_minus'
                  hash['Minus'].push(["0-", key])
              end
    end
    hash
  end

  def blood_group(b_g)
    case b_g
      when 'a_plus'
        "A+"
      when 'b_plus'
        "B+"
      when 'ab_plus'
        "AB+"
      when 'zero_plus'
        "0+"
      when 'a_minus'
        "A-"
      when 'b_minus'
        "B-"
      when 'ab_minus'
        "AB-"
      when 'zero_minus'
        "0-"
    end
  end

end
