module Employee::MedicinesHelper

  def list_packages(packages)
    packages = packages.split(';')
    html = ""
    packages.each {|package| html << content_tag(:p, package)}
    html.html_safe
  end

  def back_button_path(params)
    if params[:from_summary].present?
      summary_employee_appointment_path(params[:from_summary])
    else
      edit_employee_medicine_path(@medicine)
    end
  end
end
