module Employee::DiseasesHelper

  def build_employee_disease_breadcrumb(disease)
    content_tag :ul, class: 'breadcrumb' do
      concat content_tag :li, (link_to 'Słownik ICD-10', employee_diseases_path)
      disease.ancestors.each do |disease|
        concat content_tag :li, (link_to disease.id, employee_disease_path(disease))
      end
      concat content_tag :li, ( disease.id)
    end
  end

  def disease_tree(disease)

    tree = disease.ancestor_ids.map {|code| code}
    tree << disease.id
    tree.join('/')

  end

end
