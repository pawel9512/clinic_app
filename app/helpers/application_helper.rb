module ApplicationHelper

  def controller?(*controller)
    controller.include?(params[:controller])
  end

  def action?(*action)
    action.include?(params[:action])
  end

  def notify_status(type)
    case type
      when 'notice'
        'info'
      when 'alert'
        'danger'
      else
        type
    end
  end

  def notify_icon(type)
    case type
      when 'success'
        'fa fa-check'
      when 'info'
        'fa fa-info-circle'
      when 'warning'
        'fa fa-warning'
      when 'danger'
        'fa fa-ban'
      else
    end
  end

  def angle_back_link(path, text = 'Cofnij')
    link_to path, class: 'btn btn-labeled btn-default' do
      content_tag(:span, class: 'btn-label') do
        content_tag(:i, '', class: 'fa fa-arrow-left')
      end + text
    end
  end

  def navigation_link(path, name, icon,method='get')
    link_to path, method: method do
      concat content_tag(:em, '', class: icon)
      concat(content_tag(:span) do
        name
      end)
    end
  end

  def login_link(path, main_text, small_text, icon)
    link_to path, class: 'link-unstyled text-success' do
      concat(content_tag :em, '', class: icon)
      concat(content_tag :span, main_text, class: 'h4')
      concat(content_tag :small, small_text, class: 'text-muted')
    end
  end

  def link_to_add_row(name, f, association, **args)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.simple_fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize, f: builder)
    end
    link_to(name, '#', class: "add_fields " + args[:class], data: {id: id, fields: fields.gsub("\n", "")})
  end

end
