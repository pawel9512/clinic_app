module Patient::EmployeesHelper

  def get_work_days(employee)

    days = employee.day_schedules.order(:day).pluck(:day).uniq.map do |day|
      case day.to_sym
        when :monday
          'Poniedziałek'
        when :tuesday
          'Wtorek'
        when :wednesday
          'Środa'
        when :thursday
          'Czwartek'
        when :friday
          'Piątek'
      end
    end
    days.join(' | ')
  end
end
