module Patient::DiseasesHelper

  def build_disease_breadcrumb(disease)
    string = ''
    string += (link_to 'Słownik ICD-10', patient_diseases_path)
    string += ' / '
    disease.ancestors.each do |disease|
      string += (link_to disease.id, patient_disease_path(disease))
      string += ' / '
    end
    string += (disease.id)
    string.html_safe
  end

  def disease_tree(disease)
    tree = disease.ancestor_ids.map {|code| code}
    tree << disease.id
    tree.join('/')

  end
end
