class Employee::ReportsController < Employee::BaseController
  def doctors
    @report_filter = OpenStruct.new(doctor: report_params[:doctor], start_date: report_params[:start_date], end_date: report_params[:end_date])
    if @report_filter.doctor.present?
      authorize! :doctors, :report, Employee.find(@report_filter.doctor)
    else
      authorize! :doctors, :report
    end
    if @report_filter.doctor.present? and @report_filter.start_date.present? and @report_filter.end_date.present?
      @report = Employee::Reports::Service.get_report report_params
    end
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: 'doctors',
               disable_javascript: false,
               javascript_delay: 5000
      end
    end
  end

  def patients
    authorize! :patients, :report
    respond_to do |format|
      format.html
      format.pdf do
        @report = Employee::Reports::Service.get_patient_report report_params
        render pdf: 'patients',
               disable_javascript: false,
               javascript_delay: 5000

      end
    end

  end

  def patient_report
    @report = Employee::Reports::Service.get_patient_report report_params
    respond_to do |format|
      format.json do
        render json: @report.to_json
      end
    end
  end

  private

  def report_params
    params.fetch(:report_filter, {}).permit(:patient, :doctor, :start_date, :end_date)
  end
end
