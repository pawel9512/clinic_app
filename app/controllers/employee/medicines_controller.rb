class Employee::MedicinesController < Employee::BaseController
  authorize_resource
  before_action :find_medicine, only: [:edit, :update, :destroy, :show]


  def index
    respond_to do |format|
      format.html
      format.json {
        if params[:start].present?
          render json: MedicinesDatatable.new(view_context)
        else
          render json: Medicine.where('lower(name) like :search or lower(common_name) like :search', search: "%#{params[:selectSearch].downcase}%")
        end
      }
    end
  end

  def new
    @medicine = Medicine.new
  end

  def create
    medicine = Employee::Medicines::Manager.create medicine_params
    if medicine.present?
      respond_common_to_action employee_medicines_path, error: false
    else
      respond_common_to_action employee_medicines_path, error: true
    end

  end

  def edit
  end

  def update
    Employee::Medicines::Manager.update @medicine, medicine_params
    respond_with_success " Lek został pomyślnie zaktualizowany ", employee_medicines_path
  end

  def destroy
    Employee::Medicines::Manager.destroy @medicine
    respond_with_success " Lek został pomyślnie usunięty ", employee_medicines_path
  end

  def show
  end

  def fetch_from_csv

  end

  def parse_csv
    Employee::Medicines::Service.parse_csv_medicines(parse_csv_params)
    respond_with_success 'Baza lekarstw została zaktualizowana', employee_medicines_path
  end

  private

  def find_medicine
    @medicine = Medicine.find(params[:id])
    raise NotFound if @medicine.blank?
  end

  def medicine_params
    params.require(:medicine).permit(:name, :common_name, :strength, :active_substance, :form, :marketing_holder, :package)
  end

  def parse_csv_params
    params.permit(:medicines_update_url, :file)

  end
end
