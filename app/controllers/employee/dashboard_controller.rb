class Employee::DashboardController < Employee::BaseController
  def welcome
    if current_employee.admin?
      redirect_to employee_appointments_path
    elsif current_employee.doctor?
      redirect_to employee_employee_appointments_path current_employee
    else
      redirect_to employee_appointments_path
    end
  end
end
