class Employee::PatientsController < Employee::BaseController
  before_action :find_patient, only: [:show, :edit, :update, :destroy]
  authorize_resource
  def index
    @patients = Patient.all
    respond_to do |format|
      format.json {render json: @patients}
      format.html
    end
  end

  def create
    if Patient.find_by(email: patient_params[:email]).present?
      respond_with_error "Pacjent o podanym emailu już istnieje", employee_patients_path
    else
      Employee::Patients::Manager.create patient_params
      respond_with_success "Pacjent został pomyślnie dodany. Na podany email zostało wysłane hasło.", employee_patients_path
    end
  end

  def new
    @patient = Patient.new
  end

  def edit
  end

  def update
    response = Employee::Patients::Manager.update @patient, patient_params
    show_action_message response
  end

  def destroy
    response = @patient.destroy
    show_action_message response
  end

  def show

  end

  private
  def find_patient
    @patient = Patient.find(params[:id])
  end

  def patient_params
    params.require(:patient).permit(:first_name, :email, :last_name, :identifier, :identifier_type, :sex, :blood_group, :phone_number)
  end

  def show_action_message(response)
    if response
      respond_common_to_action employee_patients_path, error: false
    else
      respond_common_to_action employee_patients_path, error: true
    end
  end
end
