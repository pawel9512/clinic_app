class Employee::DiseasesController < Employee::BaseController
  authorize_resource
  before_action :find_disease_by_id, only: [:destroy, :show, :new, :update, :edit, :details]

  def index
    @diseases = Disease.roots.order(:code)
  end

  def show
    @diseases = @disease.children.order(:code)
  end

  def new
    if @disease.present?
      @disease = @disease.children.build
    else
      @disease = Disease.new
    end
  end

  def create
    disease = Employee::Diseases::Manager.create(disease_params)
    respond_with_success "Choroba została pomyślnie utworzona", disease.parent.present? ? employee_disease_path(disease.parent) : employee_diseases_path
  end

  def edit

  end

  def update
    disease = Employee::Diseases::Manager.update(@disease, disease_params)
    respond_with_success "Choroba została pomyślnie zaktualizowana", disease.parent.present? ? employee_disease_path(disease.parent) : employee_diseases_path
  end

  def destroy
    parent = @disease.parent
    @disease.destroy!
    respond_with_success "Choroba została pomyślnie usunięta", parent.present? ? employee_disease_path(parent) : employee_diseases_path
  end

  def details

  end

  def parse_xml
    Employee::DiseasesCategories::Manager.parse_xml import_params
    respond_with_success 'Baza chorób została zaktualizowana', employee_diseases_path
  end

  def import

  end

  def specific_diseases
    @specific_diseases = Disease.where('lower(name) like ? or lower(code) like ?', "%#{params[:selectSearch].downcase}%", "%#{params[:selectSearch].downcase}%")
    respond_to do |format|

      format.json {
        render json: @specific_diseases
      }

    end
  end

  private

  def import_params
    params.permit(:file, :disease_update_url)
  end

  def find_disease_by_id
    @disease = Disease.friendly.find(params[:id]) if params[:id].present?
  end

  def disease_params
    params.require(:disease).permit(:name, :code, :parent_id, :description)
  end
end
