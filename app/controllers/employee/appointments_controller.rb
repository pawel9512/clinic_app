class Employee::AppointmentsController < Employee::BaseController
  load_and_authorize_resource :employee, only: [:index]
  authorize_resource
  before_action :find_appointment, only: [:update, :show, :edit, :summary, :destroy, :cancel]


  def index
    if params[:type] == 'ended'
      @appointments = @employee.doctor_appointments.ended.order(actual_end: :desc)
    else
      if @current_user.doctor?
        @appointments = @employee.doctor_appointments.incoming.where(planned_start: (Time.now.beginning_of_day - 7.days)..Time.now.end_of_day + 30.days).order(:planned_start)
        # @appointments = @employee.doctor_appointments.incoming.order(:planned_start)
      else
        @appointments = @employee.doctor_appointments.incoming.where(planned_start: (Time.now.beginning_of_day - 7.days)..Time.now.end_of_day + 30.days).order(:planned_start)
      end
    end
  end

  def day_appointments
    appointments = Employee::Appointments::Service.get_day_appointments day_appointments_params
    respond_to do |format|
      format.json {render json: appointments}
    end
  end

  def appointments
  end

  def edit
    @appointment.update!(actual_start: Time.now) unless params[:from_summary].present?
  end

  def show

  end

  def update
    respond_to do |format|
      format.json {
        appointment = Employee::Appointments::Manager.update_with_reminder @appointment, appointment_json_params
        render json: appointment

      }
      format.html {
        @appointment = Employee::Appointments::Manager.update @appointment, appointment_html_params.merge(actual_end: Time.now)
        if @appointment.status == 'incoming'
          redirect_to summary_employee_appointment_path(@appointment)
        else
          respond_with_success "Wizyta została zakończona", employee_employee_appointments_path(@appointment.doctor)
        end
      }

    end
  end

  def summary
    # if @appointment.status != 'ended'
    #   respond_with_error "Wystąpił błąd podczas przeprowadzania wizyty spróbój ponownie", employee_employee_appointments_path(@appointment.doctor)
    # end
    #
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: 'summary' # Excluding ".pdf" extension.
      end
    end
  end

  def patient_appointments
    respond_to do |format|
      format.json {render json: PatientAppointmentsDatatable.new(view_context, @current_ability)}
    end
  end

  def generate_appointments
    Employee::Appointments::Manager.generate_appointmnets_for_month
    respond_with_success "Wizyty na następny miesąć zostały wygenerowane", employee_root_path
  end

  def destroy
    @appointment.destroy!
    respond_with_success "Wizyty została pomyślnie usunięta", employee_employee_appointments_path(@appointment.doctor)
  end

  def incoming
    @incoming_filter = OpenStruct.new(doctor: incoming_appointments_params[:doctor], patient: incoming_appointments_params[:patient])
    @appointments = Employee::Appointments::Service.get_incoming @incoming_filter
  end

  def cancel
    Patient::Appointments::Manager.cancel(@appointment)
    respond_with_success "Wizyty została pomyślnie odwołana", request.referrer
  end

  private

  def appointment_json_params
    params.permit(:patient_id, :description, :status)
  end

  def appointment_html_params
    params.require(:appointment).permit(:doctor, :patient, :patient_id, :doctor_comment, :description, :status, appointment_diseases_attributes: [:id, :_destroy, :disease_code], prescriptions_attributes: [:id, :_destroy, :employee_id, :patient_id, :description, prescription_medicines_attributes: [:id, :_destroy, :quantity, :comment, :medicine_id]])
  end

  def incoming_appointments_params
    params.fetch(:@incoming_filter, {}).permit(:patient, :doctor)
  end

  def find_appointment
    @appointment = Appointment.find(params[:id])
  end

  def day_appointments_params
    params.permit(:day, :doctor_id)
  end
end
