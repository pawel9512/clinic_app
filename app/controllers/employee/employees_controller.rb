class Employee::EmployeesController < Employee::BaseController
  authorize_resource
  before_action :find_employee, only: [:show, :edit, :update, :destroy, :work_schedule]

  def index
    @employees = Employee.all
  end

  def doctors
    @doctors = Employee.doctor
  end

  def show
  end

  def new
    @employee = Employee.new
  end

  def edit
  end

  def work_schedule

  end

  def create
    if Employee.find_by(email: employee_params[:email]).present?
      respond_with_error "Pracownik o podanym emailu już istnieje", employee_employees_path
    else
      Employee::Employees::Manager.create employee_params
      respond_with_success "Pracownik został pomyślnie dodany. Na podany email zostało wysłane hasło.", employee_employees_path
    end
  end

  def update
    if @employee.update(employee_params)
      respond_common_to_action employee_employees_path, error: false
    else
      respond_common_to_action employee_employees_path, error: true
    end
  end


  def destroy
    if @employee.destroy
      respond_common_to_action employee_employees_path, error: false
    else
      respond_common_to_action employee_employees_path, error: true
    end

  end

  def appointments
    @doctors = Employee.doctor.has_appointments.uniq
  end

  def appointments_doctors
    day = appointments_doctors_params[:day].to_date
    @doctors = Employee.doctor.has_appointments_in_day(day)
    respond_to do |format|
      format.json {render json: @doctors}
    end
  end

  private
  def find_employee
    @employee = Employee.find(params[:id])
    raise NotFound if @employee.blank?
  end

  def employee_params
    params.require(:employee).permit(:first_name, :email, :last_name, :pesel, :role, :phone_number, :appointment_time)
  end

  def appointments_doctors_params
    params.permit(:day)
  end
end
