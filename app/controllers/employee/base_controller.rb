class Employee::BaseController < ApplicationController
  before_action :authenticate_employee!
  before_action :check_if_employee

  def check_if_employee
    if current_patient
      flash.clear
      # if you have rails_admin. You can redirect anywhere really
      redirect_to(root_path) && return
    end
  end
end