class Employee::DaySchedulesController < Employee::BaseController
  before_action :find_employee
  before_action :find_day_schedule, only: [:edit, :update, :show, :destroy]

  def index
    @day_schedules = @employee.day_schedules

  end

  def show
  end

  def new
    @day_schedule = @employee.day_schedules.new
  end

  def create
    @day_schedule = @employee.day_schedules.new(day_schedule_params)
    @day_schedule.save!
  end

  def edit

  end

  def update
    @day_schedule.update(day_schedule_params)
  end

  def destroy
    @day_schedule.destroy
  end

  private
  def find_day_schedule
    @day_schedule = DaySchedule.find_by(id: params[:id])
  end

  def find_employee
    @employee = Employee.find_by(id: params[:employee_id])
  end

  def day_schedule_params
    params.require(:day_schedule).permit(:employee_id, :start_time, :end_time, :day)
  end
end
