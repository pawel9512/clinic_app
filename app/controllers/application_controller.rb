class ApplicationController < ActionController::Base
  # before_action :set_cache_headers
  include Exceptions
  protect_from_forgery with: :exception

  def dashboard
  end

  rescue_from NotFound, with: :not_found
  rescue_from CanCan::AccessDenied do |exception|
    render :file => "#{Rails.root}/public/403.html", :status => 403, :layout => false
  end

  protected

  def not_found(e)
    log_error e
    respond_with_error(e.message, root_path, 404)
  end

  def standard_error(e)
    render 'errors/500'
  end

  def respond_with_success(msg = '', redirect_path = nil)
    redirect_path = referer_path if redirect_path.blank?
    flash[:success] = msg
    respond_to do |format|
      format.html {redirect_to redirect_path}
      format.json {render json: {msg: msg}}
    end
  end

  def respond_with_error(msg = '', redirect_path = nil, status = 400)
    redirect_path = referer_path if redirect_path.blank?
    flash[:danger] = msg
    respond_to do |format|
      format.html {redirect_to redirect_path}
      format.json {render json: {msg: msg}, status: status}
    end
  end

  def respond_common_to_action(redirect_path = nil, error: false)
    msg = error ? 'Błąd podczas wykonywania akcji, spróbuj ponowanie' : 'Akcja została wykonana pomyślnie'
    error ? respond_with_error(msg, redirect_path) : respond_with_success(msg, redirect_path)
  end

  def current_ability
    @current_user = patient_signed_in? ? current_patient : current_employee
    @current_ability ||= Ability::Factory.build_ability_for(@current_user)
  end


  private

  # def set_cache_headers
  #   response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
  #   response.headers["Pragma"] = "no-cache"
  #   response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  # end

end
