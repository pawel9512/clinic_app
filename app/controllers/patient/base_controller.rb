class Patient::BaseController < ApplicationController
  before_action :authenticate_patient!
  layout 'patient'

  def check_if_patient
    if current_employee
      flash.clear
      # if you have rails_admin. You can redirect anywhere really
      redirect_to(root_path) && return
    end
  end
end