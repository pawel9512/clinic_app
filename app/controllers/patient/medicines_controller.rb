class Patient::MedicinesController < Patient::BaseController
  before_action :find_medicine, only: [:show]

  def index
    respond_to do |format|
      format.html
      format.json {
        if params[:start].present?
          render json: MedicinesDatatable.new(view_context)
        else
          render json: Medicine.where('lower(name) like :search or lower(common_name) like :search', search: "%#{params[:selectSearch].downcase}%")
        end
      }
    end

  end

  def show
  end

  private
  def find_medicine
    @medicine = Medicine.find(params[:id])
    raise NotFound if @medicine.blank?
  end
end
