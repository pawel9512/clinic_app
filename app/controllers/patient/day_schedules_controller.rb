class Patient::DaySchedulesController < Patient::BaseController
  before_action :find_employee

  def index
    @day_schedules = @employee.day_schedules
  end

  private
  def find_employee
    @employee = Employee.find_by(id: params[:employee_id])
  end

end
