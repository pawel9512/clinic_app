class Patient::AppointmentsController < Patient::BaseController
  authorize_resource only: [:summary]
  before_action :find_appointment, only: [:summary, :update, :cancel]

  def index
    if params[:type] == 'ended'
      @appointments = current_patient.appointments.ended.order(actual_end: :desc).page(params[:page])
    else
      @appointments = current_patient.appointments.incoming.where(planned_start: Time.now.beginning_of_day..(Time.now.+ 30.days).end_of_day).order(planned_start: :desc).page(params[:page])
    end
  end

  def edit
  end

  def summary
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: 'summary' # Excluding ".pdf" extension.
      end
    end
  end

  def update
    respond_to do |format|
      format.json {
        appointment = Employee::Appointments::Manager.update @appointment, appointment_json_params
        render json: appointment
      }
    end
  end


  def make_appointment
  end

  def day_appointments
    appointments = Employee::Appointments::Service.get_day_appointments day_appointments_params
    respond_to do |format|
      format.json {render json: appointments}
    end
  end

  def cancel
    Patient::Appointments::Manager.cancel(@appointment)
    respond_with_success "Wizyty została pomyślnie odwołana", request.referrer
  end

  private
  def find_appointment
    @appointment = Appointment.find(params[:id])
  end

  def appointment_json_params
    params.permit(:patient_id, :description, :status)
  end

  def day_appointments_params
    params.permit(:day, :doctor_id)
  end
end
