class Patient::EmployeesController < Patient::BaseController
  before_action :find_employee, only: [:show_work_schedule]

  def work_schedules
    @employees = Employee.has_appointments
  end

  def show_work_schedule
  end

  def doctors_who_has_appointments
    day = appointments_doctors_params[:day].to_date
    @doctors = Employee.doctor.has_appointments_in_day(day)
    respond_to do |format|
      format.json {render json: @doctors}
    end
  end

  private

  def find_employee
    @employee = Employee.find(params[:id])
  end

  def appointments_doctors_params
    params.permit(:day)
  end
end
