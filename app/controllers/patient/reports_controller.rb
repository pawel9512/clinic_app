class Patient::ReportsController < Patient::BaseController
  def patient_report
    @report = Employee::Reports::Service.get_patient_report report_params, true
    respond_to do |format|
      format.json do
        render json: @report.to_json
      end
    end
  end

  def patients
    authorize! :patients, :report
    respond_to do |format|
      format.html
      format.pdf do
        @report = Employee::Reports::Service.get_patient_report report_params
        render pdf: 'patients',
               disable_javascript: false,
               javascript_delay: 1000

      end
    end

  end


  private

  def report_params
    params.fetch(:report_filter, {}).permit(:patient, :doctor, :start_date, :end_date)
  end
end
