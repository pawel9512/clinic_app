class Patient::DiseasesController < Patient::BaseController
  before_action :find_disease_by_id, only: [:show, :details]

  def index
    @diseases = Disease.roots.order(:code)
  end

  def show
    @diseases = @disease.children.order(:code)
  end

  def details

  end

  def find_disease_by_id
    @disease = Disease.friendly.find(params[:id]) if params[:id].present?
  end
end
