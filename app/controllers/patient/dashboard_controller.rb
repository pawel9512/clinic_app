class Patient::DashboardController < Patient::BaseController
  def index
    redirect_to make_appointment_patient_appointments_path
  end
end
