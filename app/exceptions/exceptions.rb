module Exceptions
  class CustomException < Exception
    def message
      'Wystąpił bład podczas działania aplikajci'
    end
  end

  class NotFound < CustomException
    def message
      'Nie można było znaleźć żądanego zasobu'
    end
  end
end