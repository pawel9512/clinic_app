import React from 'react'
import ReactModal from 'react-modal';
import fontawesome from '@fortawesome/fontawesome'
import {faCalendarPlus, faCalendarTimes} from '@fortawesome/fontawesome-free-solid'
import './styles/modalStyles.css'
import moment from 'moment/src/moment';

ReactModal.setAppElement(document.getElementById('remote-container'));
fontawesome.library.add(faCalendarTimes, faCalendarPlus);

class AppointmentIcon extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    getAppointmentIcon(status) {
        switch (status) {
            case 'empty':
                return (
                    <div className="col-xs-4 text-center text-success">
                        <em className="fas fa-calendar-plus fa-2x"></em>
                    </div>
                );
            case 'incoming':
                return (
                    <div className="col-xs-4 text-center text-danger">
                        <em className="fas fa-calendar-times fa-2x"></em>
                    </div>
                );
            case 'ended':
                return (
                    <div className="col-xs-4 text-center text-warning">
                        <em className="fas fa-calendar-alt fa-2x"></em>
                    </div>
                );

        }
    }

    isAppointmentPassed(planned_start) {
        let plannedStart = new Date(planned_start).getTime();
        let currentDate = new Date().getTime();
        if (plannedStart < currentDate) {
            return true;
        }
    }


    getRenderStatus(status, planned_start) {
        if (this.isAppointmentPassed(planned_start)) {
            return 'Brak możliwości zapisu';
        }
        switch (status) {
            case 'empty':
                return 'Pusta';
            case 'incoming':
                if ((this.props.patient_id == this.props.appointment.patient_id)) {
                    return '[TWOJA] ZAPLANOWANA';
                }
                else {
                    return 'Zaplanowana';
                }
            case 'in_progress':
                return 'W trakcie';
            case 'ended':
                if ((this.props.patient_id == this.props.appointment.patient_id)) {
                    return '[TWOJA] ZAKOŃCZONA';
                }
                else {
                    return 'Zakończona';
                }
        }
    }

    render() {
        let style = {cursor: 'pointer'};
        if (this.isAppointmentPassed(this.props.appointment.planned_start)) {
            style['backgroundColor'] = 'LightGrey';
        }

        return (
            <div>
                <div onClick={() => this.props.handleClick(this.props.appointment)}
                     style={style}
                     key={this.props.appointment.id}
                     className="col-xs-6 bb bt br bl appointment-block">
                    <div className="row row-table row-flush">
                        {this.getAppointmentIcon(this.props.appointment.status)}
                        <div className="col-xs-8">
                            <div className="panel-body text-center">
                                <h4 className="mt0" style={{whiteSpace: "nowrap"}}>{this.getRenderStatus(this.props.appointment.status, this.props.appointment.planned_start)}</h4>
                                <p className="mb0 text-muted">
                                    Godzina {moment(this.props.appointment.planned_start).format('HH:mm')}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}


export default AppointmentIcon