import React from 'react'
import ReactModal from 'react-modal';
import fontawesome from '@fortawesome/fontawesome'
import {faCalendarPlus, faCalendarTimes} from '@fortawesome/fontawesome-free-solid'
import './styles/modalStyles.css'
import AppointmentIcon from './AppointmentIcon'
import AppointmentDetails from './AppointmentDetails'
import moment from 'moment/src/moment';
import swal from "sweetalert";


ReactModal.setAppElement(document.getElementById('remote-container'));
fontawesome.library.add(faCalendarTimes, faCalendarPlus);

const token = $('meta[name="csrf-token"]').attr('content');

class AppointmentLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDoctor: this.props.selectedDoctor,
            appointmentDetailsMode: false,
            showModal: false,
            selectedAppointment: null,
            appointments: this.props.appointments
        };
        this.toggleModal = this.toggleModal.bind(this);
        this.handleAppointmentClick = this.handleAppointmentClick.bind(this);
        this.changeAppointmentDetailsMode = this.changeAppointmentDetailsMode.bind(this);
        this.handelAppointmentDetialsUpdate = this.handelAppointmentDetialsUpdate.bind(this);
        this.handleAppointmentCancel = this.handleAppointmentCancel.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({appointments: nextProps.appointments})
    }

    handelAppointmentDetialsUpdate(appointment) {
        let appointments = this.state.appointments.slice();
        for (let i in appointments) {
            if (appointments[i].id == appointment.id) {
                appointments[i] = appointment
            }
        }
        this.setState({appointments})
    }

    handleAppointmentCancel() {
        const formData = {
            patient_id: null,
            description: '',
            status: 'empty'

        };
        swal({
            title: "Czy jesteś pewien?",
            text: "Za chwilę odwołasz wizytę!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                return fetch('/patient/appointments/' + this.state.selectedAppointment.id, {
                    method: 'put',
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest',
                        'X-CSRF-Token': token,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(formData),
                    credentials: 'same-origin'
                })
                    .then(response => {
                        if (response.ok) {
                            return response
                        }
                        throw new Error('Nie można było zaktualizować wizyty');
                    })
                    .then(response => {
                        return response.json()
                    })
                    .then((result) => {
                        swal({
                            title: "Zmodyfikowano wizytę",
                            text: "Wizyta została odwołana",
                            icon: "success"
                        }).then((value) => this.handelAppointmentDetialsUpdate(result));
                    })
                    .catch(error => {
                        console.log(error);
                        swal({
                            title: "Błąd",
                            text: error.message || error,
                            icon: "error"
                        })
                    })
            } else {
                swal("Operacja przerwana", "Wizyta nie została odwołana", "error");
            }
        });

    }

    handleAppointmentClick(selectedAppointment) {
        if (selectedAppointment.status == 'ended') {
            return;
        }
        if ((this.props.patient_id == selectedAppointment.patient_id) || selectedAppointment.status == 'empty') {
            this.setState({selectedAppointment}, () => this.toggleModal(selectedAppointment));
        }
    }

    toggleModal(appointment) {
        let status = appointment.status;
        let buttons = {cancel: "Powrót"};
        if (status == 'incoming') {
            buttons['cancelAppointment'] = {
                text: "Odwołaj wizytę",
                value: "cancelAppointment",
            };
            buttons['modifyAppointment'] = {
                text: "Modyfikuj wizytę",
                value: "modifyAppointment"
            };
        } else if (status = 'empty') {
            buttons['makeAppointment'] = {
                text: "Umów się na wizytę",
                value: "makeAppointment",
            }

        }
        swal("Wybierz żądaną czynność", {
            buttons: buttons,
        })
            .then((value) => {
                switch (value) {

                    case "cancelAppointment":
                        this.handleAppointmentCancel();
                        break;
                    case "modifyAppointment":
                        this.changeAppointmentDetailsMode();
                        break;
                    case "makeAppointment":
                        this.changeAppointmentDetailsMode();
                        break;

                }
            });
    }


    changeAppointmentDetailsMode() {
        this.setState({appointmentDetailsMode: !this.state.appointmentDetailsMode});
    }


    renderMainContent() {
        if (this.props.appointments.length == 0 && !this.state.appointmentDetailsMode) {
            return (
                <h3 className="text-center">Brak wizyt dla wybranych opcji</h3>

            )
        }
        if (this.state.appointmentDetailsMode) {
            return <div>
                {<AppointmentDetails appointment={this.state.selectedAppointment}
                                     changeDetailsMode={this.changeAppointmentDetailsMode}
                                     handleAppointmentDetailsUpdate={this.handelAppointmentDetialsUpdate}
                                     patient_id={this.props.patient_id}
                                     selectedDoctor={this.props.selectedDoctor}/>}
            </div>
        }
        else {
            return (
                <div className="panel b">
                    <div className="panel-heading text-center">
                        <h4>{'Lista wizyt dla dnia ' + moment(this.props.selectedDay).format('DD-MM-YY')}</h4>
                    </div>
                    <div className="panel-body">
                        <div className="row row-flush">
                            {
                                this.state.appointments.sort(function (a, b) {
                                    return new Date(a.planned_start) - new Date(b.planned_start)
                                }).map((appointment) => {
                                    return <AppointmentIcon
                                        key={appointment.id}
                                        handleClick={this.handleAppointmentClick}
                                        appointment={appointment}
                                        patient_id={this.props.patient_id}
                                    />
                                })
                            }
                        </div>

                    </div>
                </div>
            )
        }

    }

    render() {
        return (<div>
                {this.renderMainContent()}

            </div>
        )
    }

}


export default AppointmentLayout