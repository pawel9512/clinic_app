import React from 'react'
import './styles/appointmentDetails.css'
import swal from 'sweetalert';


const token = $('meta[name="csrf-token"]').attr('content');


class AppointmentDetails extends React.Component {
    constructor(props) {
        super(props);
        this.handlePatientSelect = this.handlePatientSelect.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.changeDetailsMode = this.changeDetailsMode.bind(this);
        this.state = {
            selectedDoctor: this.props.selectedDoctor,
            selectedPatient: {value: this.props.appointment.patient_id},
            appointment: this.props.appointment,
            description: this.props.appointment.description ? this.props.appointment.description : '',
            displayErrors: false
        };
    }

    changeDetailsMode() {
        this.props.changeDetailsMode()
    }

    handlePatientSelect(selectedPatient) {
        this.setState({selectedPatient})
    }

    handleDescriptionChange(e) {
        this.setState({description: e.target.value})
    }

    handleFormSubmit(e) {
        e.preventDefault();
        if (!e.target.checkValidity()) {
            this.setState({displayErrors: true});
            return;
        }
        this.setState({displayErrors: false});
        const formData = {
            patient_id: this.props.patient_id,
            description: this.state.description,
            status: 'incoming'

        };
        return fetch('/patient/appointments/' + this.state.appointment.id, {
            method: 'put',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-Token': token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData),
            credentials: 'same-origin'
        })
            .then(response => {
                if (response.ok) {
                    return response
                }
                throw new Error('Nie można było zaktualizować wizyty');
            })
            .then(response => {
                return response.json()
            })
            .then((result) => {
                this.setState({appointment: result});
                swal({
                    title: "Zmodyfikowano wizytę",
                    text: "Pacjent został zapisane na wizytę",
                    icon: "success"
                }).then(() => {
                    this.props.handleAppointmentDetailsUpdate(this.state.appointment);
                    this.changeDetailsMode()
                });
            })
            .catch(error => {
                swal({
                    title: "Błąd",
                    text: error.message,
                    type: "error"
                })
            })

    }

    render() {
        var value = this.state.selectedPatient && this.state.selectedPatient.value;
        const {displayErrors} = this.state
        return (
            <div className="panel b">
                <div className="panel-heading">
                    <div className="clearfix">
                        <div className="pull-left">
                            <h4>Szczegóły wizyty</h4>
                        </div>
                        <div className="pull-right">
                            <button onClick={this.props.changeDetailsMode} type="button"
                                    className="btn btn-labeled btn-default">
                                <span className="btn-label">
                                    <i className="fa fa-arrow-left"></i>
                                </span>Cofnij
                            </button>
                        </div>
                    </div>
                </div>
                <div className="panel-body">
                    <form className="form-horizontal" onSubmit={this.handleFormSubmit}>
                        <div className="form-group">
                            <label className="col-sm-2 control-label">Lekarz</label>
                            <div className="col-sm-10 ">
                                <h4>{this.props.selectedDoctor.label}</h4>
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="col-sm-2 control-label">Notatki</label>
                            <div className="col-sm-10">
                                <textarea
                                    onChange={this.handleDescriptionChange}
                                    value={this.state.description}
                                    className="form-control" row="5"></textarea>
                            </div>
                        </div>
                        <div className="text-center">
                            <button className="btn btn-success"> Aktualizuj</button>
                        </div>
                    </form>
                </div>
            </div>

        )
    }

}


export default AppointmentDetails