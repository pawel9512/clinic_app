import React from 'react';
import DayPicker from 'react-day-picker'
import 'react-day-picker/lib/style.css'
import {Async} from 'react-select';
import 'react-select/dist/react-select.css'
import AppointmentLayout from './AppointmentLayout'


class AppointmentsLayout extends React.Component {
    constructor(props) {
        super(props);
        this.handleDayClick = this.handleDayClick.bind(this);
        this.handleDoctorSelect = this.handleDoctorSelect.bind(this);
        this.getAppointments = this.getAppointments.bind(this);
        this.getDoctorsOptions = this.getDoctorsOptions.bind(this);
        this.state = {
            selectedDay: new Date(),
            appointments: [],
            selectedDoctor: {value: 0, label: ""},
        };
    }

    getAppointments() {
        console.log('elo patient get appointments');
        if (this.state.selectedDoctor == null || this.state.selectedDay == null) {
            return
        }
        const formData = {
            day: this.state.selectedDay,
            doctor_id: this.state.selectedDoctor.value
        };
        var token = $('meta[name="csrf-token"]').attr('content');
        fetch('/patient/appointments/day-appointments', {
            method: 'post',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-Token': token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData),
            credentials: 'same-origin'
        }).then(response => response.json())
            .then(response => {
                this.setState({appointments: response})
            })
    }

    getDoctorsOptions(input) {
        const formData = {
            day: this.state.selectedDay,
        };
        var token = $('meta[name="csrf-token"]').attr('content');
        return fetch('/patient/employees/doctors-who-has-appointments.json', {
            method: 'post',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-Token': token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData),
            credentials: 'same-origin'
        })
            .then(response => {
                return response.json()
            })
            .then(json => {
                return json.map(doctor => {
                    return {
                        value: doctor.id,
                        label: doctor.first_name + ' ' + doctor.last_name
                    }
                })
            })
            .then(options => {
                return {options: options}
            })
    }


    handleDayClick(day, modifiers = {}) {
        if (modifiers.disabled) {
            return;
        }
        this.setState({
            selectedDay: modifiers.selected ? undefined : day
        }, () => {
            this.refs.selectDoctor.loadOptions("");
            this.getAppointments();
        })
    }

    handleDoctorSelect(selectedDoctor) {
        this.setState({selectedDoctor}, () => {
            this.getAppointments()
        })
    }


    render() {
        const {selectedDoctor} = this.state;
        const value = selectedDoctor && selectedDoctor.value;
        let today = new Date();
        let nextMonth = new Date(today.getFullYear(), today.getMonth() + 1, today.getDate());

        return (
            <div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="panel b">
                            <div className="panel-heading">
                                <div className="panel-title text-center">Wybierz dzień wizyty</div>
                            </div>
                            <div className="panel-body text-center">
                                <DayPicker selectedDays={this.state.selectedDay} onDayClick={this.handleDayClick}
                                           disabledDays={[{before: today, after: nextMonth}, {daysOfWeek: [0, 6]}]}/>
                            </div>
                        </div>
                        <div className="panel b">
                            <div className="panel-heading">
                                <div className="panel-title text-center">Wybierz lekarza</div>
                            </div>
                            <div className="panel-body">
                                <Async
                                    cache={false}
                                    ref="selectDoctor"
                                    clearable={false}
                                    backspaceRemoves={false}
                                    deleteRemoves={false}
                                    multi={false}
                                    value={value}
                                    onChange={this.handleDoctorSelect}
                                    loadOptions={this.getDoctorsOptions}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-8">

                        <AppointmentLayout selectedDay={this.state.selectedDay}
                                           selectedDoctor={selectedDoctor}
                                           appointments={this.state.appointments}
                                           patient_id={this.props.patient_id}
                        />
                    </div>
                </div>

            </div>
        );
    }
}

export default AppointmentsLayout