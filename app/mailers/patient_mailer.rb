class PatientMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.patient_mailer.after_create.subject
  #
  def after_create(patient_id, password)
    @password = password
    @patient = Patient.find(patient_id)
    mail to: @patient.email
  end

  def appointment_reminder(appointment_id, patient_id)
    @appointment = Appointment.find(appointment_id)
    return if @appointment.blank? or @appointment.status.to_sym != :incoming or @appointment.patient.id != patient_id
    mail to: 'pawek35@gmail.com'
  end

end
