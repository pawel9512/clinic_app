class EmployeeMailer < ApplicationMailer
  def after_create(employee_id, password)
    @password = password
    @employee = Employee.find(employee_id)
    mail to: @employee.email
  end
end
