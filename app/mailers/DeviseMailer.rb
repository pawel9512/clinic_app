class DeviseMailer < Devise::Mailer
  helper :application
  include Devise::Controllers::UrlHelpers
  default template_path: 'devise/mailer'
  default from: 'noreply@auth.personate.io'

  def confirmation_instructions(record, token, opts={})
    super
  end

end