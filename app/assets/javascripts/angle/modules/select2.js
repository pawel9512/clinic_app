// Select2
// -----------------------------------

(function (window, document, $, undefined) {

    $(function () {

        if (!$.fn.select2) return;

        // Select 2

        $('.select2').select2({
            theme: 'bootstrap'
        });
// EMPLOYEE APPOINTMENT PROCESS
        $('.appointment-medicines-select2').select2({
            minimumInputLength: 3,
            theme: 'bootstrap',
            ajax: {
                delay: 250,
                url: '/employee/medicines.json',
                dataType: 'json',
                data: function (params) {
                    query = {
                        selectSearch: params.term,
                        type: 'public'
                    }
                    return query
                },
                processResults: function (medicines) {

                    return {
                        results: medicines.map(function (medicine) {
                            return {
                                id: medicine.id,
                                text: medicine.name + ' | ' + medicine.strength + ' | ' + medicine.form
                            }
                        })
                    };
                },
            }
        });

        $('.appointment-disease-select2').select2({
            minimumInputLength: 3,
            width: '100%',
            theme: 'bootstrap',
            ajax: {
                delay: 250,
                url: '/employee/specific-diseases.json',
                dataType: 'json',
                data: function (params) {
                    query = {
                        selectSearch: params.term,
                        type: 'public'
                    }
                    return query
                },
                processResults: function (diseases) {

                    return {
                        results: diseases.map(function (disease) {
                            return {
                                id: disease.id,
                                text: disease.name
                            }
                        })
                    };
                },
            }
        });

        // Appointments incoming

        $('#_incoming_filter_doctor').select2({
            width: '100%',
            theme: 'bootstrap',
        });
        $('#_incoming_filter_patient').select2({
            width: '100%',
            theme: 'bootstrap'
        });

        // Reports doctors
        $('#report_filter_doctor').select2({
            width: '100%',
            theme: 'bootstrap'
        });
        // Reports patients
        $('#report_filter_patient').select2({
            width: '100%',
            theme: 'bootstrap'
        });

    });

})(window, document, window.jQuery);

