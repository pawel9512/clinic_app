// Demo datatables
// -----------------------------------


(function (window, document, $, undefined) {

    if (!$.fn.dataTable) return;
    //Employees

    $(function () {
        initTable($('#employeesTable'));
        initTable($('#doctorsTable'));
        initTable($('#doctorsWithAppointmentsTable'));
        initTable($('#disease-categories-table'));
        initTable($('#disease-table'));
        initTable($('#doctorRaportTable'));

        initTable($('#doctorAppointmentsTable'));
        initServerSlideTable($('#medicinesTable'));


        // Patients
        initServerSlideTable($('#patientMedicinesTable'));
        initServerSlideTable($('#patientAppointmentsTable'));
        $('#patientEmployeesSchedulesTable').dataTable(
        );
        initTable($('#patientDiseasesTable'));
        initTable($('#patientsTable'));
        // Appointments#index
        initTable($('#patientAllAppointmentsTable'));

    });

    function initServerSlideTable(table) {
        table.dataTable({
            'processing': true,
            'serverSide': true,
            'ajax': table.data('url'),
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Polish.json"
            }
        });
    }

    function initTable(table) {
        $.fn.dataTable.moment('D-MM-YYYY HH:mm');
        table.dataTable({
            'paging': true,  // Table pagination
            'ordering': true,  // Column ordering
            'info': true,  // Bottom left status text
            'responsive': true, // https://datatables.net/extensions/responsive/examples/
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Polish.json"
            }
        });

    }

    function initTableWithoutEverything(table) {
        table.dataTable({
            'paging': true,  // Table pagination
            'ordering': false,  // Column ordering
            'info': true,  // Bottom left status text
            'responsive': true, // https://datatables.net/extensions/responsive/examples/
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Polish.json"
            }
        });

    }


})(window, document, window.jQuery);
