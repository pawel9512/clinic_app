// Custom jQuery
// -----------------------------------


(function (window, document, $, undefined) {

    if (!$.fn.fullCalendar) return;

    // When dom ready, init calendar and events
    $(function () {

        // The element that will display the calendar
        initEmployeeScheduleCalendar();
        initReceptionistsScheduleCalendar($('#receptionist-schedule-calendar'), 'employee');
        initReceptionistsScheduleCalendar($('#patient-employee-schedule-calendar'), 'patient');

    });


    /**
     * Invoke full calendar plugin and attach behavior
     * @param  jQuery [calElement] The calendar dom element wrapped into jQuery
     * @param  EventObject [events] An object with the event list to load when the calendar displays
     */
    function initEmployeeScheduleCalendar() {
        var calendar = $('#employee-schedule-calendar');
        let slotDuration = calendar.data('employee-appointment-time');
        console.log(slotDuration);
        calendar.fullCalendar({
            minTime: "07:00:00",
            maxTime: "19:00:00",
            slotDuration: {minutes: slotDuration},
            header: {
                center: 'title',
                right: '',
                left: ''
            },
            title: 'qeqwe',
            defaultView: 'agendaWeek',
            selectable: true,
            selectHelper: true,
            editable: true,
            height: 'auto',
            eventLimit: true,
            allDaySlot: false,
            weekends: false,
            timezone:'UTC',
            columnFormat: 'dddd',
            slotLabelFormat: 'H:mm',
            timeFormat: 'H:mm',
            titleFormat: '[Harmonogram pracownika]',
            slotLabelInterval: {minutes: 30},
            eventSources: "/employee/employees/" + calendar.data('employee-id') + "/day_schedules.json",

            select: function (start, end) {
                $.getScript("/employee/employees/" + calendar.data('employee-id') + "/day_schedules/new", function () {
                    $('#day_schedule_start').val(moment(start).format("HH:mm"));
                    $('#day_schedule_end').val(moment(end).format("HH:mm"));
                    $('#day_schedule_day').val(moment(start).format('dddd').toLowerCase());
                });

                calendar.fullCalendar('unselect');
            },
            eventDrop: function (event, delta, revertFunc) {
                params = {
                    day_schedule: {
                        start_time: event.start.format("HH:mm"),
                        end_time: event.end.format("HH:mm"),
                        day: moment(event.start).format('dddd').toLowerCase()
                    }
                };
                alertScheduleChange(event, params, revertFunc);
            },
            eventResize: function (event, delta, revertFunc) {
                alertScheduleChange(event, revertFunc);
                params = {
                    day_schedule: {
                        start_time: event.start.format("HH:mm"),
                        end_time: event.end.format("HH:mm"),
                        day: moment(event.start).format('dddd').toLowerCase()
                    }
                };
                alertScheduleChange(event, params, revertFunc);

            },
            eventAllow: function (dropInfo, event) {
                var start = new Date(dropInfo.start);
                var end = new Date(dropInfo.end);
                if (start.getDay() == end.getDay()) {
                    return true;
                } else {
                    return false;
                }
            },
            eventClick: function (event, jsEvent, view) {
                $.getScript(event.edit_url, function () {
                    $('#day_schedule_start').val(event.start.format("HH:mm"));
                    $('#day_schedule_end').val(event.end.format("HH:mm"));
                    $('#day_schedule_day').val(event.day);
                })
            }

        })
    }

    function updateSchedule(event, params) {
        $.ajax({
            url: event.update_url,
            data: params,
            dataType: 'json',
            context: this,
            type: 'put'
        })
    }

    function initReceptionistsScheduleCalendar(calendar, user_type) {
        calendar.fullCalendar({
            minTime: "07:00:00",
            maxTime: "19:00:00",
            header: {
                center: 'title',
                right: '',
                left: ''
            },
            title: 'qeqwe',
            defaultView: 'agendaWeek',
            eventLimit: true,
            allDaySlot: false,
            height: 'auto',
            weekends: false,
            columnFormat: 'dddd',
            slotLabelFormat: 'H:mm',
            timeFormat: 'H:mm',
            titleFormat: '[Harmonogram pracownika]',
            eventSources: "/" + user_type + "/employees/" + calendar.data('employee-id') + "/day_schedules.json",
        })
    }

    function alertScheduleChange(event, params, revertFunc) {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        })

        swalWithBootstrapButtons({
            title: 'Czy chcesz zmienić godzinę wizyty?',
            text: "Start: " + event.start.format("HH:mm") + " Zakończenie: " + event.end.format("HH:mm"),
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Tak',
            cancelButtonText: 'Nie, anuluj zmiany',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                updateSchedule(event, params);
                swalWithBootstrapButtons(
                    'Akcja udana',
                    'Harmonogram został zmieniony',
                    'success'
                )
            } else if (
                // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons(
                    'Akcja przerwana',
                    'Harmonogram nie został zmieniony',
                    'error'
                );
                revertFunc();
            }

        });
    }


})(window, document, window.jQuery);


