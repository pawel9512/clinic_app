// Forms Demo
// -----------------------------------


(function (window, document, $, undefined) {

    $(function () {
        // DATETIMEPICKER
        // -----------------------------------

        if ($.fn.datetimepicker) {

            $('#datetimepicker2').datetimepicker({
                format: 'LT'
            });

        }

    });

})(window, document, window.jQuery);