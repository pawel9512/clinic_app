//--- Moment
//= require moment/min/moment-with-locales.min
//= require chart.js/dist/Chart.js

//--- Datatables
//= require datatables.net/js/jquery.dataTables.js
//= require datatables.net-bs/js/dataTables.bootstrap.js
//= require datatables.net-buttons/js/dataTables.buttons.js
//= require datatables.net-buttons-bs/js/buttons.bootstrap.js
//= require datatables.net-responsive/js/dataTables.responsive.js
//= require datatables.net-responsive-bs/js/responsive.bootstrap.js
// Datatable buttons Column visibility
//= require datatables.net-buttons/js/buttons.colVis.js


//--- Moment and datatable date
//= require moment/min/moment-with-locales.min
//= require datatable-sorting-datetime-moment/index.js

const token = $('meta[name="csrf-token"]').attr('content');
var doctorsDistributionChart;
var patientAppointmentsDistributionChart;


$('#patient_report_submit').click(function (event) {
    let patient = $('#report_filter_patient');
    let start_date = $('#report_filter_start_date');
    let end_date = $('#report_filter_end_date');
    if (!patient.val() || !start_date.val() || !end_date.val()) {
        $.notify('Aby pobrać raport uzupełnij brakujące dane', {status: 'danger'});
    } else {
        return fetch('/patient/reports/patient_report', {
            method: 'POST',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-Token': token,
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                report_filter: {
                    patient: patient.val(),
                    start_date: start_date.val(),
                    end_date: end_date.val()
                }
            }),
            credentials: 'same-origin'

        }).then(response => {

            return response.json();
        }).then(response => {
            if ($.fn.DataTable.isDataTable("#patientDiseasesStatistics")) {
                $("#patientDiseasesStatistics").DataTable().clear().destroy();
            }
            if ($.fn.DataTable.isDataTable("#patientMedicinesStatistics")) {
                $("#patientMedicinesStatistics").DataTable().clear().destroy();
            }
            fillStatistics(response);
            setPatientAppointmentsDistributionChart(response.appointments_per_day);
            fillpatientMedicinesStatisticsTable(response.diseases_details.frequently_prescribed_medicine);
            fillpatientDiseasesStatisticsTable(response.diseases_details.frequently_diagnosed_diseases);
            initTable($('#patientDiseasesStatistics'));
            initTable($('#patientMedicinesStatistics'));
            let pdfButton = $('#generatePatientReportPdf');
            pdfButton.empty();
            pdfButton.append("<a class='btn btn-success pull-right align-center' target='_blank' href='/patient/reports/patients.pdf?report_filter%5Bpatient%5D=" + patient.val() + "&amp;report_filter%5Bend_date%5D=" + end_date.val() + "&amp;report_filter%5Bstart_date%5D=" + start_date.val() + "'>Generuj PDF</a>")
        })

    }
});

function fillStatistics(response) {
    let selectedPatient = $('#report_filter_patient :selected');
    let start_date = $('#report_filter_start_date');
    let end_date = $('#report_filter_end_date');
    $("#staticticsPatient").text(selectedPatient.text());
    $("#staticticsDateRange").text(start_date.val() + ' do ' + end_date.val());
    $("#staticticsVistsCount").text(response.main_statistics.all_appointments_count);
    $("#staticticsDiagnosedDiseases").text(response.main_statistics.diagnosed_diseases_count);
    $("#staticticsPrescriptionsCount").text(response.main_statistics.prescriptions_count);
    $("#staticticsMedicinesCount").text(response.main_statistics.medicines_count);
    setPatientDoctorAppointmentsChart(response.main_statistics.appointments_per_doctor);
}

function setPatientDoctorAppointmentsChart(doctorsDistribution) {
    let doctorNames = Object.keys(doctorsDistribution);
    let doctorVisitsCount = Object.values(doctorsDistribution);
    let ctx = document.getElementById("doctorsAppointmentsDistributionChart").getContext('2d');
    if (doctorsDistributionChart != null) {
        doctorsDistributionChart.destroy();
    }
    doctorsDistributionChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: doctorNames,
            datasets: [{
                label: '# of Votes',
                data: doctorVisitsCount,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 110, 30, 0.2)',
                    'rgba(255, 180, 90, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 110, 30, 1)',
                    'rgba(255, 180, 90, 0.2)'
                ],
                borderWidth: 1
            }]
        }
    });

}

function setPatientAppointmentsDistributionChart(appointmentsDistribution) {

    let selectedPatient = $('#report_filter_patient :selected');
    let timeFormat = 'DD/MM/YYYY';

    let labels = Object.keys(appointmentsDistribution);
    let data = Object.values(appointmentsDistribution);
    let ctx = document.getElementById("patientAppointmentsDistributionChart").getContext('2d');
    let cfg = {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: selectedPatient.text(),
                data: data,
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
            }]
        },
        options: {
            responsive: true,
            scales: {
                yAxes: [{
                    ticks: {
                        stepSize: 1
                    }
                }]
            }
        }
    };
    if (patientAppointmentsDistributionChart != null) {
        patientAppointmentsDistributionChart.destroy();
    }
    patientAppointmentsDistributionChart = new Chart(ctx, cfg);

}

function fillpatientMedicinesStatisticsTable(medicines) {
    $("#patientMedicinesStatistics > tbody").empty();
    let $table = document.getElementById('patientMedicinesStatistics').getElementsByTagName('tbody')[0];
    medicines.forEach(function (medicine, index) {
        let row = $table.insertRow(index);
        let cell1 = row.insertCell(0);
        let cell2 = row.insertCell(1);
        let cell3 = row.insertCell(2);
        cell1.innerHTML = medicine.name;
        cell2.innerHTML = medicine.count;
        cell3.innerHTML = "<a target='_blank' href='" + medicine.url + "'>" + "Szczegóły" + "</a>";
    })


}

function fillpatientDiseasesStatisticsTable(diseases) {
    $("#patientDiseasesStatistics > tbody").empty();
    let $table = document.getElementById('patientDiseasesStatistics').getElementsByTagName('tbody')[0];
    diseases.forEach(function (disease, index) {
        let row = $table.insertRow(index);
        let cell1 = row.insertCell(0);
        let cell2 = row.insertCell(1);
        let cell3 = row.insertCell(2);
        cell1.innerHTML = disease.name;
        cell2.innerHTML = disease.count;
        cell3.innerHTML = "<a target='_blank' href='" + disease.url + "'>" + "Szczegóły" + "</a>";
    })


}

function initTable(table) {
    table.dataTable({
        'paging': true,  // Table pagination
        'ordering': true,  // Column ordering
        'info': true,  // Bottom left status text
        'responsive': true, // https://datatables.net/extensions/responsive/examples/
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Polish.json"
        }
    });

}

