// Place all the behaviors and hooks related to the matching controller here.


//--- Sparklines
//= require jquery-sparkline/jquery.sparkline.min
//--- Datatables
//= require datatables.net/js/jquery.dataTables.js
//= require datatables.net-bs/js/dataTables.bootstrap.js
//= require datatables.net-buttons/js/dataTables.buttons.js
//= require datatables.net-buttons-bs/js/buttons.bootstrap.js
//= require datatables.net-responsive/js/dataTables.responsive.js
//= require datatables.net-responsive-bs/js/responsive.bootstrap.js
// Datatable buttons Column visibility
//= require datatables.net-buttons/js/buttons.colVis.js
// Validate
//= require parsleyjs/dist/parsley.min
//= require jquery-validation/dist/jquery.validate
//= require parsleyjs/dist/i18n/pl.js

// Select
//= require select2/dist/js/select2

//--- jQuery UI
//= require components-jqueryui/jquery-ui
//= require jqueryui-touch-punch/jquery.ui.touch-punch.min
//--- Moment and datatable date
//= require moment/min/moment-with-locales.min
//= require datatable-sorting-datetime-moment/index.js

//--- Fullcalendar
//= require fullcalendar/dist/fullcalendar.js
//= require fullcalendar/dist/gcal
//--- DatetimePicker
//= require eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min

//= require sweetalert2/dist/sweetalert2.js

function roleChanged() {
    let employeeRole = $('#employee_role option:selected').text();
    if (employeeRole != 'Doctor') {
        $('#employeeRole').hide();
    }
    else {
        $('#employeeRole').show();
    }
}

roleChanged();
$('#employee_role').change(function (e) {
    roleChanged();
});

