//--- Datatables
//= require datatables.net/js/jquery.dataTables.js
//= require datatables.net-bs/js/dataTables.bootstrap.js
//= require datatables.net-buttons/js/dataTables.buttons.js
//= require datatables.net-buttons-bs/js/buttons.bootstrap.js
//= require datatables.net-responsive/js/dataTables.responsive.js
//= require datatables.net-responsive-bs/js/responsive.bootstrap.js
// Datatable buttons Column visibility
//= require datatables.net-buttons/js/buttons.colVis.js

//--- Moment and datatable date
//= require moment/min/moment-with-locales.min
//= require datatable-sorting-datetime-moment/index.js

// --- Select2
//= require select2/dist/js/select2

//--- Validate
//= require parsleyjs/dist/parsley.min
//= require jquery-validation/dist/jquery.validate
//= require parsleyjs/dist/i18n/pl.js


//= require sweetalert2/dist/sweetalert2.js



$('.form-appointment-edit').bind('cocoon:before-insert', function (e, nested_form) {
    unique_id = 'appointment-prescription' + new Date().getTime();
    prescriptionMedicinesUniqueId = 'prescriptionMedicines' + new Date().getTime();
    nested_form.find('a').attr('href', '#' + unique_id);
    nested_form.find('.panel-collapse').attr('id', unique_id);
    nested_form.find('.append-medicine-button').attr('data-association-insertion-node', '#' + prescriptionMedicinesUniqueId);
    nested_form.find('.appointment-prescription-medicines').attr('id', prescriptionMedicinesUniqueId);
    nested_form.find('.appointment-medicines-select2').each(function () {
        $(this).select2({
            minimumInputLength: 3,
            theme: 'bootstrap',
            ajax: {
                delay: 250,
                url: '/employee/medicines.json',
                dataType: 'json',
                data: function (params) {
                    query = {
                        selectSearch: params.term,
                        type: 'public'
                    }
                    return query
                },
                processResults: function (medicines) {

                    return {
                        results: medicines.map(function (medicine) {
                            return {
                                id: medicine.id,
                                text: medicine.name + ' | ' + medicine.strength + ' | ' + medicine.form
                            }
                        })
                    };
                },
            }
        });
    });
    nested_form.find('.appointment-disease-select2').each(function () {
        $(this).select2({
            minimumInputLength: 3,
            width: '100%',
            theme: 'bootstrap',
            ajax: {
                delay: 250,
                url: '/employee/diseases/specific-diseases.json',
                dataType: 'json',
                data: function (params) {
                    query = {
                        selectSearch: params.term,
                        type: 'public'
                    }
                    return query
                },
                processResults: function (diseases) {

                    return {
                        results: diseases.map(function (disease) {
                            return {
                                id: disease.code,
                                text: disease.name,
                            }
                        })
                    };
                },
            }
        });
    });

});