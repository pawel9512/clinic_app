//--- Filestyle
//= require bootstrap-filestyle/src/bootstrap-filestyle
//--- Datatables
//= require datatables.net/js/jquery.dataTables.js
//= require datatables.net-bs/js/dataTables.bootstrap.js
//= require datatables.net-buttons/js/dataTables.buttons.js
//= require datatables.net-buttons-bs/js/buttons.bootstrap.js
//= require datatables.net-responsive/js/dataTables.responsive.js
//= require datatables.net-responsive-bs/js/responsive.bootstrap.js
// Datatable buttons Column visibility
//= require datatables.net-buttons/js/buttons.colVis.js
// Validate
//= require parsleyjs/dist/parsley.min
//= require jquery-validation/dist/jquery.validate

//--- Moment and datatable date
//= require moment/min/moment-with-locales.min
//= require datatable-sorting-datetime-moment/index.js