module Patient::Appointments
  module Manager
    class << self

      def cancel(appointment)
        appointment.status = :empty
        appointment.description = nil
        appointment.patient = nil
        appointment.save!
        appointment
      end

    end
  end
end