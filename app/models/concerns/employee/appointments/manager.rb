module Employee::Appointments
  module Manager
    class << self
      def update(appointment, appointments_params)
        appointment.update!(appointments_params)
        appointment
      end

      def update_with_reminder(appointment, appointments_params)
        appointment.assign_attributes(appointments_params)
        if (appointments_params[:status].to_sym == :incoming or appointment.patient_id_changed?) and appointment.days_to_appointment >= 1
          PatientMailer.delay_until(appointment.planned_start - 1.day).appointment_reminder(appointment.id, appointment.patient.id)
        end
        appointment.save!
        appointment
      end

      def generate_appointmnets_for_month
        doctors = Employee.all.doctor
        today = Date.today
        (today..today + 30.days).each do |day|
          day_name = day.strftime("%A").downcase
          next if day_name == 'saturday' or day_name == 'sunday'
          doctors.each do |doctor|
            schedules_for_day = doctor.day_schedules.where(day: day_name)
            schedules_for_day = schedules_for_day.sort_by {|schedule| schedule.start_time.strftime("%H:%M")}
            schedules_for_day.each do |schedule|
              start_time = schedule.start_time
              next if Appointment.where('doctor_id = :doctor_id and planned_start::date = :date', doctor_id: doctor.id, date: Date.new(day.year, day.month, day.day)).present?
              while start_time < schedule.end_time do
                appointment_start = Time.new(day.year, day.month, day.day, start_time.hour, start_time.min)
                Appointment.find_or_create_by!(doctor: doctor, planned_start: appointment_start, planned_end: appointment_start + doctor.appointment_time.minutes, status: :empty)
                start_time += doctor.appointment_time.minutes

              end
            end
          end

        end
      end
    end
  end
end