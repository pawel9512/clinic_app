  module Employee::Appointments
  module Service
    class << self
      def get_day_appointments(params)
        day = params[:day].to_datetime
        Appointment.where(planned_start: day.midnight..day.end_of_day, doctor: params[:doctor_id])
      end

      def get_incoming(params)
        appointments = Appointment.incoming.where(planned_start: Time.now.beginning_of_day..(Time.now + 30.days).end_of_day)
        appointments = appointments.where(doctor: params.doctor) if params.doctor.present?
        appointments = appointments.where(patient: params.patient) if params.patient.present?
        appointments
      end
    end
  end
end