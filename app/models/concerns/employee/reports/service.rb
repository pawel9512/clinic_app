module Employee::Reports
  module Service
    class << self
      include Rails.application.routes.url_helpers

      def get_report(report_params)
        doctor = report_params[:doctor]
        start_date = report_params[:start_date].to_date.beginning_of_day
        end_date = report_params[:end_date].to_date.end_of_day
        {
            doctor_name: Employee.find(report_params[:doctor]).full_name.to_s,
            doctor_appointments: Appointment.ended.by_doctor(doctor).by_date(start_date, end_date),
            doctor_appointments_per_day: get_doctor_appointments_per_day(doctor, start_date, end_date),
            doctor_diagnosed_diseases_count: Appointment.joins(:appointment_diseases).ended.where(doctor: doctor, actual_start: start_date..end_date).count,
            doctor_prescription_count: Appointment.joins(:prescriptions).ended.where(doctor: doctor, actual_start: start_date..end_date).count,
            all_appointments_count: Appointment.ended.by_date(start_date, end_date).count
        }
      end

      def get_patient_report(report_params, is_patient_scope = false)
        patient = report_params[:patient]
        start_date = report_params[:start_date].to_date.beginning_of_day
        end_date = report_params[:end_date].to_date.end_of_day
        {
            patient: Patient.find(patient),
            start_date: start_date,
            end_date: end_date,
            appointments_per_day: get_patient_appointments_per_day(patient, start_date, end_date),
            main_statistics: {
                appointments_per_doctor: get_patient_appointments_per_doctor(patient, start_date, end_date),
                all_appointments_count: Appointment.ended.by_patient(patient).by_date(start_date, end_date).count,
                diagnosed_diseases_count: Appointment.joins(:appointment_diseases).ended.where(patient: patient, actual_start: start_date..end_date).count,
                prescriptions_count: Appointment.joins(:prescriptions).ended.where(patient: patient, actual_start: start_date..end_date).count,
                medicines_count: Appointment.joins(prescriptions: [:prescription_medicines]).ended.by_patient(patient).by_date(start_date, end_date).count

            },
            diseases_details: {
                frequently_diagnosed_diseases: get_patient_diseases(patient, start_date, end_date, is_patient_scope),
                frequently_prescribed_medicine: get_patient_medicines(patient, start_date, end_date, is_patient_scope)
            }


        }
      end

      private

      def get_patient_appointments_per_day(patient, start_date, end_date)
        hash = {}
        (start_date.to_datetime..end_date.to_datetime).each do |day|
          hash[day.strftime("%d-%m-%Y")] = Appointment.ended.by_patient(patient).where(actual_start: day.beginning_of_day..day.end_of_day).count
        end
        hash
      end

      def get_patient_appointments_per_doctor(patient, start_date, end_date)
        appointments_per_doctor = Appointment.joins(:doctor).ended.by_patient(patient).where(actual_start: start_date.beginning_of_day..end_date.end_of_day).group('first_name', 'last_name').count('id')
        appointments_per_doctor.keys.each do |key_hash|
          appointments_per_doctor[key_hash.join(' ')] = appointments_per_doctor.delete key_hash
        end
        appointments_per_doctor
      end


      def get_doctor_appointments_per_day(doctor, start_date, end_date)
        (start_date.to_datetime..end_date.to_datetime).map do |day|
          {
              day: day.strftime("%d-%m-%Y"),
              appointments_count: Appointment.ended.by_doctor(doctor).where(actual_start: day.beginning_of_day..day.end_of_day).count
          }

        end
      end

      def get_patient_diseases(patient, start_date, end_date, is_patient_scope)
        diseases = Appointment.joins(:appointment_diseases).ended.by_patient(patient).by_date(start_date, end_date).group('disease_code').order('count_id desc').count('id')
        diseases.keys.map do |disease_code|
          d = Disease.find(disease_code)
          {
              name: d.name,
              count: diseases[disease_code],
              url: is_patient_scope ? details_patient_diseases_path(d) : details_employee_diseases_path(d)

          }
        end
      end

      def get_patient_medicines(patient, start_date, end_date, is_patient_scope)
        medicines = Appointment.joins(prescriptions: [:prescription_medicines]).ended.by_patient(patient).by_date(start_date, end_date).group('prescription_medicines.medicine_id').order('count_id desc').count('id')
        medicines.keys.map do |medicine_id|
          m = Medicine.find(medicine_id)
          {
              name: m.name,
              count: medicines[medicine_id],
              url: is_patient_scope ? patient_medicine_path(medicine_id) : employee_medicine_path(medicine_id)

          }
        end
      end

    end
  end
end
