module Employee::Employees
  module Manager
    class << self
      def create(employee_params)
        generated_password = Devise.friendly_token.first(8)
        employee_params.merge!(:password => generated_password)
        employee = Employee.create!(employee_params)
        EmployeeMailer.after_create(employee.id, generated_password).deliver_later if employee.present?
        employee
      end
    end
  end
end