module Employee::SpecificDiseases
  module Manager
    class << self
      def create(main_disease, specific_disease_params)
        main_disease.specific_diseases.create!(specific_disease_params)
      end

      def update(specific_disease, specific_disease_params)
        specific_disease.update!(specific_disease_params)
        specific_disease
      end

      def destroy(specific_disease)
        specific_disease.destroy!
      end
    end
  end
end