module Employee::Patients
  module Manager
    class << self
      def create(patient_params)
        password = Devise.friendly_token.first(8)
        patient_params.merge!(password: password)
        patient = Patient.create!(patient_params)
        PatientMailer.after_create(patient.id, password).deliver_later if patient.present?
        patient
      end

      def update(patient, patient_params)
        patient.update patient_params
      end
    end
  end
end