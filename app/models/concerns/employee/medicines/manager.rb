module Employee::Medicines
  module Manager
    class << self
      def create(medicine_params)
        Medicine.create!(medicine_params)
      end

      def update(medicine, medicine_params)
        medicine.update!(medicine_params)
        medicine
      end

      def destroy(medicine)
        medicine.destroy
      end
    end
  end
end
