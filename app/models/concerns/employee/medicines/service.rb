module Employee::Medicines
  module Service
    class << self
      require 'open-uri'
      require 'csv'
      require 'activerecord-import'
      require 'tempfile'
      require 'fileutils'

      def parse_csv_medicines(params)
        csv_file = params[:medicines_update_url].present? ? open(params[:medicines_update_url]) : params[:file]

        modify_csv(csv_file.path)
        medicines = []
        ids = []
        Medicine.transaction do
          CSV.foreach(csv_file.path, encoding: "utf-8", headers: :true, col_sep: "|", quote_char: '"', header_converters: :symbol) do |row|
            medicine = row.to_hash
            ids << medicine[:identyfikator_produktu_leczniczego]
            medicines << Medicine.new(id: medicine[:identyfikator_produktu_leczniczego],
                                      name: medicine[:nazwa_produktu_leczniczego],
                                      common_name: medicine[:nazwa_powszechnie_stosowana],
                                      strength: medicine[:moc],
                                      form: medicine[:posta_farmaceutyczna],
                                      marketing_holder: medicine[:podmiot_odpowiedzialny],
                                      package: medicine[:opakowania],
                                      active_substance: medicine[:substancja_czynna],
                                      leaflet_link: medicine[:ulotka],
                                      characteristic_link: medicine[:charakterystyka])
          end
          Medicine.import(medicines, on_duplicate_key_update: {conflict_target: [:id], columns: [:name,
                                                                                                 :common_name,
                                                                                                 :strength,
                                                                                                 :form,
                                                                                                 :marketing_holder,
                                                                                                 :package,
                                                                                                 :active_substance,
                                                                                                 :leaflet_link,
                                                                                                 :characteristic_link]})
        end
      end

      def modify_csv(file)
        temp_file = Tempfile.new('temp')
        begin
          File.readlines(file).each do |line|
            line = line[1...-3]
            line.gsub!(/"\|"/, "|")
            line.gsub!(/"/, "'")
            temp_file << line + "\n"
          end
          temp_file.close
          FileUtils.mv(temp_file.path, file)
        ensure
          temp_file.close
          temp_file.unlink
        end
      end

    end
  end
end
