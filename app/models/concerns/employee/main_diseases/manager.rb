module Employee::MainDiseases
  module Manager
    class << self
      def create(diseases_subcategory, main_disease_params)
        diseases_subcategory.main_diseases.create!(main_disease_params)
      end

      def update(main_disease, main_disease_params)
        main_disease.update!(main_disease_params)
        main_disease
      end

      def destroy(main_disease)
        main_disease.destroy!
      end
    end
  end
end