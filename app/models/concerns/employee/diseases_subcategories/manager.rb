module Employee::DiseasesSubcategories
  module Manager
    class << self
      def create(diseases_category, diseases_subcategory_params)
        diseases_category.diseases_subcategories.create!(diseases_subcategory_params)
      end

      def update(diseases_subcategory, diseases_subcategory_params)
        diseases_subcategory.update!(diseases_subcategory_params)
        diseases_subcategory
      end

      def destroy(diseases_subcategory)
        diseases_subcategory.destroy!
      end
    end
  end
end