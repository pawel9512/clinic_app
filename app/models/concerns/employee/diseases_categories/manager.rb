module Employee::DiseasesCategories
  module Manager
    class << self
      require 'open-uri'
      require 'activerecord-import'

      def parse_xml(params)
        ActiveRecord::Base.transaction do
          xml_file = params[:disease_update_url].present? ? open(params[:disease_update_url]) : params[:file]
          doc = File.open(xml_file.path) {|f| Nokogiri::XML(f, nil, "UTF-8")}
          doc.xpath('//hcd/nodes/node').each_with_index do |node, index|
            code = node.attribute('code').text
            code.gsub!(/–/, "-")
            code.gsub!(/†/, "")
            code.gsub!('*', "")
            name = node.xpath('name').text
            # disease = Disease.create!(name: name, code: code)
            disease = Disease.find_or_create_by!(code: code)
            disease.update_attributes!(name: name)
            nested_node(node, disease)
          end
        end
      end

      def nested_node(node, parent_disease = nil, type: :node)
        node.xpath('./nodes/node').each do |child|
          code = child.attribute('code').text
          code.gsub!(/–/, "-")
          code.gsub!(/†/, "")
          code.gsub!('*', "")
          name = child.xpath('name').text
          if code.blank? and name.blank?
            nested_node(child, parent_disease)
          elsif code.blank?
            parent_disease.description = parent_disease.description.presence || ''
            if type == :description
              parent_disease.description += "•#{name}\n"
              parent_disease.save!
              nested_node(child, parent_disease)
            else
              parent_disease.description += "#{name}\n"
              parent_disease.save!
              nested_node(child, parent_disease, type: :description)
            end
          else
            disease = if parent_disease.nil?
                        # Disease.create!(name: name, code: code)
                        d = Disease.find_or_create_by!(code: code)
                        d.update_attributes!(name: name)
                        d
                      else
                        if parent_disease.code == code
                          parent_disease
                        elsif code.match?(/^\.\d+$/)
                          parent_disease
                        elsif code.match?(/^\d+$/)
                          parent_disease
                        else
                          # parent_disease.children.create!(name: name, code: code)
                          d = parent_disease.children.find_or_create_by!(code: code)
                          d.update_attributes!(name: name)
                          d
                        end
                      end

            nested_node(child, disease)
          end

        end
      end


      def create(diseases_category_params)
        DiseasesCategory.create!(diseases_category_params)
      end

      def update(diseases_category, diseases_category_params)
        diseases_category.update!(diseases_category_params)
        diseases_category
      end

      def destroy(diseases_category)
        diseases_category.destroy!
      end
    end
  end
end