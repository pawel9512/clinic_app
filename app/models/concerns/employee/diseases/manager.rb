module Employee::Diseases
  module Manager
    class << self
      def create(disease_params)
        Disease.create!(disease_params)
      end

      def update(disease, disease_params)
        disease.update!(disease_params)
        disease
      end
    end
  end
end