module Ability
  class Base
    include CanCan::Ability
    attr_accessor :user

    def initialize(user)
      @user = user
      alias_action :create, :read, :update, :destroy, :to => :crud
      execute
    end

    def execute
      self.protected_methods.each do |method|
        self.send(method) if self.respond_to? method, true
      end
      # and return self
      self
    end

    # A kind of Interface...
    protected

    # API SECTION

  end
end
