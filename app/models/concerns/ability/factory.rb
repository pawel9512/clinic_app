module Ability
  module Factory
    class << self
      def build_ability_for(user)
        case user
          when Patient
            Ability::Role::Patient.new user
          when Employee
            if user.admin?
              Ability::Role::Admin.new user
            elsif user.doctor?
              Ability::Role::Doctor.new user
            else
              Ability::Role::Receptionist.new user
            end
        end
      end
    end
  end
end