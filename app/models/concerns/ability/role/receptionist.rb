module Ability
  module Role
    class Receptionist < Base
      protected
      def appointments_abilities
        can [:appointments, :update, :day_appointments, :patient_appointments, :incoming], Appointment
        can :cancel, Appointment do |appointment|
          (appointment.incoming? and appointment.planned_start >= Time.now)
        end

      end

      def employees_abilities
        can [:appointments_doctors, :doctors, :work_schedule], Employee
      end

      def patient_abilities
        can [:index, :create, :update, :show], ::Patient
      end

      def report_abilities

        can :patients, :report


      end


    end
  end
end
