module Ability
  module Role
    class Doctor < Base

      protected

      def medicines_abilities
        can [:index, :show], Medicine
      end

      def diseases_abilities
        can [:index, :show, :specific_diseases, :details], Disease
      end

      def employee
        can :show, Employee do |employee|
          @user == employee
        end
      end

      def appointmnets
        can [:index, :patient_appointments], Appointment
        can :summary, Appointment do |appointment|
          appointment.status == 'ended' or (@user.doctor_appointments.exists?(appointment.id))
        end
        can :edit, Appointment do |appointment|
          @user.doctor_appointments.exists?(appointment.id) and appointment.status == 'incoming'
        end

        can :update, Appointment do |appointment|
          @user.doctor_appointments.exists?(appointment.id) and appointment.status == 'incoming'
        end


      end

      def patient_abilities
        can [:index, :show], ::Patient
      end

      def report_abilities
        can :doctors, :report do |sym, doctor|
          if doctor.blank?
            true
          else
            doctor == @user
          end

        end
        can :patients, :report


      end
    end
  end
end
