module Ability
  module Role
    class Admin < Base

      protected
      def medicines_abilities
        can :manage, Medicine
      end

      def diseases_abilities
        can :manage, Disease
      end

      def patient_abilities
        can :manage, ::Patient
      end

      def employees_abilities
        can :manage, Employee
      end

      def appointmnets_abilities
        can [:crud, :day_appointments, :generate_appointments, :incoming, :patient_appointments, :summary, :appointments], Appointment
        cannot :delete, Appointment do |appointment|
          (appointment.ended? or appointment.planned_start > Date.today)
        end
        can :cancel, Appointment do |appointment|
          (appointment.incoming? and appointment.planned_start > Time.now.beginning_of_day)
        end


      end

      def report_abilities
        can :doctors, :report
        can :patients, :report
      end
    end
  end
end
