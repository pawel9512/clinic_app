module Ability
  module Role
    class Patient < Base
      protected
      def appointmnets_abilities
        can :summary, Appointment do |appointment|
          @user.appointments.exists?(appointment.id) and appointment.status == 'ended'
        end
        can :cancel, Appointment do |appointment|
          (appointment.incoming? and appointment.planned_start >= Time.now and appointment.patient == @user)
        end
      end

      def report_abilities
        can :patients, :report do |sym, patient|
          if patient.blank?
            true
          else
            patient == @user
          end

        end

      end

      def medicines_abilities
        can :read, Medicine
      end

      def diseases_abilities
        can [:index, :show, :specific_diseases, :details], Disease
      end

    end
  end
end
