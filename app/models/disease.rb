class Disease < ApplicationRecord
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged, :sequence_separator => "_"
  # after_create :build_path
  # after_update :build_path
  has_ancestry
  self.primary_key = :code
  has_many :appointment_diseases, foreign_key: :disease_code, dependent: :destroy

  private

  def slug_candidates
    [
        [self.parent.try(:slug), :code]
    ]
  end

  def build_path
    self.children.each {|child| child.update!(slug: nil)}
  end

  protected
  def should_generate_new_friendly_id?
    code_changed? || slug_changed?
  end
end
