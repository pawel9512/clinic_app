class AppointmentDisease < ApplicationRecord
  belongs_to :disease, foreign_key: :disease_code, primary_key: :code
  belongs_to :appointment
end
