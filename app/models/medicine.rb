class Medicine < ApplicationRecord
  has_many :prescription_medicines, dependent: :destroy

  def name_with_details
    "#{name} | #{strength} | #{form}"
  end
end
