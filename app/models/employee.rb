class Employee < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :async
  enum role: [:doctor, :admin, :receptionist]
  has_many :employments
  has_many :day_schedules, dependent: :destroy
  has_many :doctor_appointments, class_name: 'Appointment', foreign_key: 'doctor_id', dependent: :destroy
  has_many :receptionist_appointments, class_name: 'Appointment', foreign_key: 'receptionist_id', dependent: :destroy

  has_many :prescriptions

  def name_with_initial
    "#{name } #{surname}"
  end

  scope :like, ->(args) {where("name LIKE ? OR surname LIKE ?", "%#{args}%", "%#{args}%")}

  scope :not_employmented, -> {joins("LEFT JOIN employments ON employees.id=employments.employee_id").where("employments.employee_id IS NULL")}
  scope :has_appointments_in_day, ->(date) {joins(:doctor_appointments).where('appointments.planned_start BETWEEN ? AND ? ', date.beginning_of_day, date.end_of_day).uniq}
  scope :has_appointments, -> {joins(:doctor_appointments).distinct}
  def full_name
    "#{first_name} #{last_name}"
  end

end
