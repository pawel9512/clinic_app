class DaySchedule < ActiveRecord::Base
  belongs_to :employee
  enum day: [:monday, :tuesday, :wednesday, :thursday, :friday]
end
