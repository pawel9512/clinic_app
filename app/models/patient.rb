class Patient < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :addresses
  has_many :appointments, dependent: :destroy
  has_many :prescriptions, dependent: :destroy
  enum sex: [:male, :female]
  enum identifier_type: [:pesel]
  enum blood_group: [:a_plus, :b_plus, :ab_plus, :zero_plus, :a_minus, :b_minus, :ab_minus, :zero_minus]

  def full_name
    "#{first_name} #{last_name}"
  end
end
