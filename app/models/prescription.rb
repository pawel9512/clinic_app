class Prescription < ApplicationRecord
  belongs_to :appointment, optional: true
  belongs_to :patient
  belongs_to :employee
  has_many :prescription_medicines, dependent: :destroy
  has_many :medicines, through: :prescription_medicines
  accepts_nested_attributes_for :prescription_medicines, allow_destroy: true

end
