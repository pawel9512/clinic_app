class Appointment < ApplicationRecord
  belongs_to :patient, optional: true
  has_many :prescriptions, dependent: :destroy
  belongs_to :doctor, class_name: 'Employee'
  belongs_to :receptionist, class_name: 'Employee', optional: true
  has_many :appointment_diseases, dependent: :destroy
  has_many :diseases, through: :appointment_diseases, dependent: :destroy
  enum status: [:empty, :incoming, :ended]
  accepts_nested_attributes_for :prescriptions, allow_destroy: true
  accepts_nested_attributes_for :appointment_diseases, allow_destroy: true

  scope :by_date, ->(start_date, end_date) {where(actual_start: start_date..end_date)}
  scope :by_doctor, ->(doctor_id) {where(doctor: doctor_id)}
  scope :by_patient, ->(patient_id) {where(patient: patient_id)}


  def days_to_appointment
    ((Time.now - self.planned_start) / 1.day).to_i
  end

  def medicines
    medicines_id = PrescriptionMedicine.where(prescription_id: self.prescriptions.map {|p| p.id}).map {|pm| pm.medicine_id}
    Medicine.where(id:medicines_id)
  end

end
