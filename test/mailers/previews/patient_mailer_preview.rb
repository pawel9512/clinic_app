class PatientMailerPreview < ActionMailer::Preview
  def appointment_reminder
    appointment_id = Appointment.where(status: :incoming).first.id
    PatientMailer.appointment_reminder(appointment_id)
  end
end