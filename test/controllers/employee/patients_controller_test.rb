require 'test_helper'

class Employee::PatientsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get employee_patients_index_url
    assert_response :success
  end

  test "should get create" do
    get employee_patients_create_url
    assert_response :success
  end

  test "should get new" do
    get employee_patients_new_url
    assert_response :success
  end

  test "should get edit" do
    get employee_patients_edit_url
    assert_response :success
  end

  test "should get update" do
    get employee_patients_update_url
    assert_response :success
  end

  test "should get destroy" do
    get employee_patients_destroy_url
    assert_response :success
  end

end
