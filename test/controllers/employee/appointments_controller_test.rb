require 'test_helper'

class Employee::AppointmentsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get employee_appointments_index_url
    assert_response :success
  end

end
