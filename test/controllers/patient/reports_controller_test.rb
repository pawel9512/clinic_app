require 'test_helper'

class Patient::ReportsControllerTest < ActionDispatch::IntegrationTest
  test "should get patients" do
    get patient_reports_patients_url
    assert_response :success
  end

  test "should get patient_report" do
    get patient_reports_patient_report_url
    assert_response :success
  end

end
