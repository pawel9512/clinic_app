# Clinic app

App was developed in 2019 as an engineering thesis. Project is an app for small private medical facilities which are on the beginning of the business lifecycle. App will provide basic functionalities to manage medical business.  
App is divided into four types of users - admin, doctor, receptionist and patient. Depending on used role, user will have acces to diferent actions.  
Acces to specific functions are authorized by cancancan. When user tries to obtain data which is not allowed for him, the error will be raised.
# Live demo
  [Link](https://cryptic-temple-17267.herokuapp.com/)
## Credentials:  
**Admin** - admin@wp.pl  
**Doctor1** - doctor1@wp.pl  
**Doctor2** - doctor2@wp.pl  
**Patient1** - patient1@wp.pl  
**Patient2** - patient2@wp.pl  
**Receptionist** - receptionist@wp.pl  

**Password**: qweqweqwe
  
## Functionalities provided for admin
* Adding new employees to the system
* Defining work schedule for employees
* Editing medicines and diseases databse.
## Functionalities provided for doctor
* Can preview appointments assigned to him
* Generating pdf/diagrams raports for patients and for himself
* Previews patient details, his appointments and presriptions history
* Conducts medical appointments  
## Functionalities provided for receptionist
* Adds new patients
* Medical appointments management (assign, edit, delete)
* Browsing doctors' work schedule
* Generating pdf/diagrams reports for patients
## Functionalities provided for patients
* Browsing appointments calendar, patient sees if appointment is empty or reserved
* Makes an appointment to see the doctor
* Browsing diseases and medicines database
* Browsing doctors' work schedule
* Checking his history of treatment
# Some interesting features that was developed in the project
* Appointmets calendar was based on React.js. User can choose appointment to book from a displayed grid.
* Admin can define doctor shedule work. Base on that shedule admin can generate appointments for next 30 days. To build work grid there was used full_calendar plugin.
* Admin can inport medicines database from csv file and diseases database from xml file. There were developed some utilities which parse that file and put them into the database. For example there are 20000 records in medicines database. Records are inserted into the database by activerecord-import gem. In addition that significant amount of record requires smart filtering and paginating. For that purpose there was used a DataTable js plugin and some custom rails clases for fetching records into tables.
* Patient has access to appointments history and their summmaries. User can generate pdf with the appointment summary. For generating pdf files there was used WickedPdf gem.
* All actions are authorized by CanCanCan gem. Abilities are divided into 4 roles. Each role has own class where authorization conditions are specified.
* There are some views where diagrams are being drawn. To draw diagrams ther was used Chart.js
# Used gems and libraries:  
1. Devise
2. CanCanCan
3. Webpacker
4. React 
5. Activerecord-import
6. Ancestry (diseases ICD10 have tree structure)
7. Chart.js
8. Datatables.js
9. FullCalendar
10. Sidekiq (for sending emails)
